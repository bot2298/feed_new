<?php

use App\Http\Resources\ExportBatchesResource;
use App\Http\Resources\ReportsExportResource;
use App\Models\Batches;
use App\Models\Timetables;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $timetables = ExportBatchesResource::collection(Batches::latest()->get())->toJson();
//    dd(json_decode($timetables));
    return view('reportMixers',['data'=>collect(json_decode($timetables))]);
});
