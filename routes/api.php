<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login')->name('login');

Route::get('login', function () {
    return response()->json(['error' => 'Unauthenticated.'], 401);
});
Route::get('unauthorized', 'UserController@unauthorized');
Route::get('export', 'API\ReportsController@export');
Route::get('export/batch', 'API\ReportsController@exportRecipes');
Route::get('export/ration', 'API\ReportsController@exportRation');
Route::get('export/1_C/{date}', 'API\ReportsController@exportOneC');
Route::get('export/2_C', 'API\ReportsController@exportTwoC');


Route::middleware('auth:api')->group(function () {

    Route::resource('mixers', 'API\MixersController');
    Route::resource('housing', 'API\HousingsController');
    Route::resource('silages', 'API\SilagesController');
    Route::resource('providers', 'API\ProvidersController');
    Route::resource('ingredients', 'API\IngredientsController');
    Route::resource('retentions', 'API\RetentionsController');
    Route::resource('write-off', 'API\WriteOffController');
    Route::resource('recipes', 'API\RecipesController');
    Route::resource('timetables', 'API\TimetablesController');
    Route::resource('users', 'API\UsersController');
    Route::resource('farms', 'API\FarmsController');
    Route::resource('rations/list', 'API\RationListController');
    Route::resource('rations', 'API\RationController');
    Route::resource('done-products', 'API\DoneProductController');
    Route::resource('reports', 'API\ReportsController');


    Route::get('lists/{type}', 'API\ListsController@index');
    Route::get('logs/{type}', 'API\LogController@index');

    Route::put('timetables/premix/{id}', 'API\TimetablesController@premixUpdate');
    Route::get('permission/sections', 'API\PermissionsController@index');
    Route::get('permission/sections/{id}', 'API\PermissionsController@show');
    Route::post('permission', 'API\PermissionsController@store');
    Route::put('permission', 'API\PermissionsController@update');

    Route::get('permission/parse', 'API\PermissionsController@parse');


//    //mobile
    Route::get('manager/mixers', 'API\Mobile\MixersController@index');
    Route::put('manager/mixers/{mixer_id}/calibration', 'API\Mobile\MixersController@update');
    Route::get('tracker/animalfeed/schedules', 'API\Mobile\SchedulesController@index');
    Route::get('tracker/animalfeed/schedules/{id}', 'API\Mobile\SchedulesController@show');
    Route::post('tracker/animalfeed/schedules/{id}/load', 'API\Mobile\SchedulesController@store');
    Route::put('tracker/animalfeed/offline/schedules/{id}/load/{batches_id}', 'API\Mobile\SchedulesController@update');
    Route::put('tracker/animalfeed/schedules/{id}/load/{batches_id}', 'API\Mobile\SchedulesController@updating');


//    Route::resource('graph', 'API\GraphController');

});

