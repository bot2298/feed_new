<?php
$style ="text-align:center;vertical-align:center;";
$th = $style."border: 1px solid #000000;background-color: #1da643;color:#ffffff";
$td = $style."border: 1px solid #000000;";

?>
<table class="table">
    <thead>
    <tr>
        <th style="{{$th}}" colspan="11"  >Інформація по виробництву готової продукції</th>
    </tr>
    <tr>
        <th style="{{$style}}">Дата виробництва</th>
        <th style="{{$style}}">ID_ виробництва</th>
        <th style="{{$style}}">ID_ готової продукції</th>
        <th style="{{$style}}">Кількість готової продукції</th>
        <th style="{{$style}}">ID_специфікації</th>
        <th style="{{$style}}">Назва специфікації</th>
        <th style="{{$style}}">ID_Склад_ГП</th>
        <th style="{{$style}}">ID_інгредієнта</th>
        <th style="{{$style}}">Кількість інгредієнта</th>
        <th style="{{$style}}">Коефіцієнт входу в готову продукцію</th>
        <th style="{{$style}}">ID_Склад інгредієнта</th>
    </thead>
    <tbody>
    @foreach($data as $timetable)
        @foreach($timetable->ingredients_id as $id)
        <tr>
            <td style="{{$td}}" >{{ \Carbon\Carbon::create($timetable->date)->format('Y-m-d') }}</td>
            <td style="{{$td}}" >{{ $timetable->id }}</td>
            <td style="{{$td}}" >{{ collect($timetable->done_prod)->implode(', ') }}</td>
            <td style="{{$td}}" >{{ $timetable->weight_done }}</td>
            <td style="{{$td}}" >{{ $timetable->recipe_id }}</td>
            <td style="{{$td}}" >{{ collect($timetable->recipe_name)->implode(' ') }}</td>
            <td style="{{$td}}" >{{ $timetable->storage_done_id }}</td>
            <td style="{{$td}}" >{{ $timetable->ingredient_code[$id-1] }}</td>
            <td style="{{$td}}" >{{ $timetable->ingredient_fact[$loop->index] }}</td>
            <td style="{{$td}}" >{{ collect($timetable->ingredient_rate[$loop->index])->implode(' ') }}</td>
            <td style="{{$td}}" >{{$timetable->storage_ingredient_id }}</td>
        </tr>
        @endforeach
    @endforeach
    </tbody>
</table>
