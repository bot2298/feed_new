<?php
$style ="text-align:center;vertical-align:center;";
$th = $style."border: 1px solid #000000;background-color: #1da643;color:#ffffff";
$td = $style."border: 1px solid #000000;";

?>
<table class="table">
    <thead>
    <tr>
        <th style="{{$th}}" colspan="11"  >Інформація по рецептурі(специфікації) готової продукції згідно якої відбулось виробництво корму</th>
    </tr>
    <tr>
        <th style="{{$style}}">ID_специфікації</th>
        <th style="{{$style}}">Назва специфікації</th>
        <th style="{{$style}}">ID_ готової продукції</th>
        <th style="{{$style}}">Назва готової продукції</th>
        <th style="{{$style}}">Кількість готової продукції</th>
        <th style="{{$style}}">ID_інгредієнта</th>
        <th style="{{$style}}">Назва інгредієнта</th>
        <th style="{{$style}}">Кількість інгредієнта</th>
        <th style="{{$style}}">Коефіцієнт входу в готову продукцію</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $recipe)
        @foreach($recipe->ingredient_id as $id)
            <tr>
                <td style="{{$td}}" >{{ $recipe->id }}</td>
                <td style="{{$td}}" >{{ $recipe->name }}</td>
                <td style="{{$td}}" >{{ collect($recipe->done_prod_id)->implode(', ') }}</td>
                <td style="{{$td}}" >{{ $recipe->done_prod_name }}</td>
                <td style="{{$td}}" >1000</td>
                <td style="{{$td}}" >{{ $id }}</td>
                <td style="{{$td}}" >{{$recipe->ingredient_name[$loop->index]}}</td>
                <td style="{{$td}}" >{{10*$recipe->ingredient_rate[$loop->index]}}</td>
                <td style="{{$td}}" >{{$recipe->ingredient_rate[$loop->index]}}</td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>
