<?php
$style ="text-align:center;vertical-align:center;";
$th = $style."border: 1px solid #000000;background-color: #1da643;color:#ffffff";
$td = $style."border: 1px solid #000000;";

?>


<table class="table">
    <thead>
    <tr>
        <th style="{{$style}}" colspan="11"  >Завантаження міксера</th>
    </tr>
    <tr>
        <th style="{{$style}}">Дата : {{\Carbon\Carbon::now()->format('d.m.Y')}}</th>
    </tr>
    <tr>
        <th style="{{$th}}">Дата виробництва</th>
        <th style="{{$th}}">Раціон</th>
        <th style="{{$th}}">Рецепт</th>
        <th style="{{$th}}">Корпус</th>
        <th style="{{$th}}">Силос</th>
        <th style="{{$th}}">План маса[кг]</th>
        <th style="{{$th}}">Факт маса[кг]</th>
        <th style="{{$th}}">Відх.[кг]</th>
        <th style="{{$th}}">Відх.[%]</th>


    </thead>
    <tbody>
    @foreach($second as $timetable)
        @if($timetable->plan != 0)
            <tr>
                <td style="{{$td}}" >{{\Carbon\Carbon::create($timetable->from)->format('d.m.Y')}}@if(!is_null($timetable->to)) - {{\Carbon\Carbon::create($timetable->to)->format('d.m.Y')}} @endif</td>
                <td style="{{$td}}" >{{collect($timetable->name)->implode('')}}</td>
                <td style="{{$td}}" >{{collect($timetable->recipes)->implode(', ')}}</td>
                <td style="{{$td}}" >{{collect($timetable->silages)->implode(', ')}}</td>
                <td style="{{$td}}" >{{collect($timetable->houses)->implode(', ')}}</td>
                <td style="{{$td}}" >{{$timetable->plan}}</td>
                <td style="{{$td}}" >{{$timetable->fact}}</td>
                <td style="{{$td}}" >{{$timetable->fact - $timetable->plan}}</td>
                @if($timetable->plan == 0)
                    <td style="{{$td}}" >0</td>
                @else
                    <td style="{{$td}}" >{{round(abs($timetable->fact - $timetable->plan)/$timetable->plan, 3)}}</td>
                @endif
            </tr>
        @endif
    @endforeach
    <tr></tr>
        <tr>
            <th style="{{$th}}">Дата виробництва</th>
            <th style="{{$th}}">Раціон</th>
            <th style="{{$th}}">Рецепт</th>
            <th style="{{$th}}">Інгредієнт</th>
            <th style="{{$th}}">План маса[кг]</th>
            <th style="{{$th}}">Факт маса[кг]</th>
            <th style="{{$th}}">Відх.[кг]</th>
        </tr>
        @foreach($data as $timetable)
            @foreach($timetable->ingredient as $id)
                <tr>
                    <td style="{{$td}}" >{{ \Carbon\Carbon::create($timetable->date)->format('d.m.Y') }}</td>
                    <td style="{{$td}}" >{{ $timetable->ration }}</td>
                    <td style="{{$td}}" >{{ $timetable->recipe }}</td>
                    <td style="{{$td}}" >{{ $timetable->ing_name[$loop->index] }}</td>
                    <td style="{{$td}}" >{{ $timetable->ing_plan[$loop->index] }}</td>
                    <td style="{{$td}}" >{{ $timetable->ing_fact[$loop->index] }}</td>
                    <td style="{{$td}}" >{{ $timetable->def[$loop->index] }}</td>
                </tr>
            @endforeach
        @endforeach

    </tbody>

</table>
