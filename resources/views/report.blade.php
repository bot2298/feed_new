
<table  border="1" cellpadding="4" cellspacing="0" class="table">
    <thead>
    <tr>
        <th style="text-align: center;vertical-align: center;" colspan="14"  >Звіт</th>
    </tr>
    <tr>
        <th style="text-align: center;vertical-align: center;" colspan="14" >Виробництво  з по </th>
    </tr>
    <tr>
        <th colspan="14" ></th>
    </tr>
    <tr>
        <th style="text-align: center;vertical-align: center;" rowspan="2">Дата</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2">Время</th>
        <th style="text-align: center;vertical-align: center;" colspan="2">Час</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2">Код</th>
        <th style="text-align: center;vertical-align: center;" colspan="3">Рацион (інгрідієнти)</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2"> Залишок, кг</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2">Силос</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2">Корпус</th>
        <th style="text-align: center;vertical-align: center;" colspan="2">Вага</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2">+/-</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2">Оператор</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2">Водій</th>
        <th style="text-align: center;vertical-align: center;" rowspan="2">Примітка</th>
    </tr>
    <tr>
        <th style="text-align: center;vertical-align: center;">вивантаження, ч/хв</th>
        <th style="text-align: center;vertical-align: center;">змішування, хв</th>
        <th style="text-align: center;vertical-align: center;">назва раціону</th>
        <th style="text-align: center;vertical-align: center;">примітка</th>
        <th style="text-align: center;vertical-align: center;">дата вводу</th>
        <th style="text-align: center;vertical-align: center;">План</th>
        <th style="text-align: center;vertical-align: center;">Факт</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $timetable)
        <tr>
            <td style="text-align: center;vertical-align: center;" >{{ \Carbon\Carbon::create($timetable->date)->format('d-m-Y') }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ \Carbon\Carbon::create($timetable->date)->format('H:i') }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ $timetable->load_time }}</td>
            <td style="text-align: center;vertical-align: center;" >{{--$timetable->mixing_time--}}-</td>
            <td style="text-align: center;vertical-align: center;" >{{ $timetable->recipe_id }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ $timetable->recipe_name }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ $timetable->comment }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ \Carbon\Carbon::create($timetable->recipe_start)->format('d-m-Y') }}</td>
            <td style="text-align: center;vertical-align: center;" >0</td>
            <td style="text-align: center;vertical-align: center;" >{{ collect($timetable->silages)->implode(', ') }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ collect($timetable->houses)->implode(', ') }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ $timetable->plan_weight }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ $timetable->fact_weight }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ $timetable->fact_weight-$timetable->plan_weight }}</td>
            <td style="text-align: center;vertical-align: center;" >{{ $timetable->operator }}</td>
            <td style="text-align: center;vertical-align: center;" >{{collect($timetable->streets)->implode(', ')}}</td>
            <td style="text-align: center;vertical-align: center;" ></td>
        </tr>
    @endforeach
    </tbody>
</table>

