<?php
$style ="text-align:center;vertical-align:center;";
$th = $style."border: 1px solid #000000;background-color: #1da643;color:#ffffff";
$td = $style."border: 1px solid #000000;background-color: #dbeff3";
?>
{{--#c5d89c--}}
<table class="table">
    <thead>
    <tr>
        <th style="{{$style}}" colspan="11">ЗВІТ СТАНУ ВИРОБНИЦТВА ККЗ</th>
    </tr>
    <tr>
        <th></th>
        <th>Date: {{Illuminate\Support\Facades\Session::get('time')}}</th>
        <th colspan="11" ></th>
    </tr>
    <tr>
        <th colspan="11" ></th>
    </tr>
    <tr>
        <th></th>
        <th style="{{$th}}">Рецепт</th>
        <th style="{{$th}}">Раціон</th>
        <th style="{{$th}}">Корпус</th>
        <th style="{{$th}}">Силос</th>
        <th style="{{$th}}">План/факт маса [кг]</th>
        <th style="{{$th}}">Відх. ± [кг]/[%]</th>
        <th style="{{$th}}">Початок [час]</th>
        <th style="{{$th}}">Тривалість [хв]</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key => $value)
        <tr>
            @if($value->status == 'Done')
                <?php $td = $style."border: 1px solid #000000;background-color: #c5d89c"?>
            @endif
            <th style="{{$th}}" >{{ $key+1 }}</th>
            <td style="{{$td}}" >{{ $value->recipe_name }}</td>
            <td style="{{$td}}" >{{ $value->ration_name }}</td>
            <td style="{{$td}}" >{{ $value->house_names}}</td>
            <td style="{{$td}}" >{{ $value->silages_number}}</td>
            <td style="{{$td}}" >{{ $value->plan_weight }}/{{ $value->fact_weight }}</td>
            <td style="{{$td}}" >{{ $value->fact_weight-$value->plan_weight }}/{{ $value->diff}}</td>
            <td style="{{$td}}" >{{ $value->start}}</td>
            <td style="{{$td}}" >{{ $value->duration }}</td>
        </tr>
    @endforeach
    @foreach($data as $key => $value)
        <tr>
            <th colspan="11" ></th>
        </tr>
        <tr>
            <th colspan="11" ></th>
        </tr>
        <tr>
            <th style="{{$th}}" colspan="3">{{ $value->recipe_name }}</th>
        </tr>
        <tr>
            <th style="{{$th}}" >{{$key+1}}</th>
            <th style="{{$th}}" >Інгредієнт</th>
            <th style="{{$th}}" >План маса[кг]</th>
            <th style="{{$th}}" >Факт маса[кг]</th>
            <th style="{{$th}}" >Відх.[кг]</th>
            <th style="{{$th}}" >Тривалість[хв]</th>
            <th style="{{$th}}" >Оператор</th>
        </tr>
        @foreach($value->ingredients as $key => $ingredient)
        <tr>
            <td style="" ></td>
            <td style="vertical-align: center;border: 1px solid #000000;" >{{$ingredient->ingredient_name}}</td>
            <td style="vertical-align: center;border: 1px solid #000000;" >{{ $ingredient->plan_weight }}</td>
            <td style="vertical-align: center;border: 1px solid #000000;" >{{ $ingredient->fact_weight }}</td>
            <td style="vertical-align: center;border: 1px solid #000000;" >{{ round($ingredient->fact_weight-$ingredient->plan_weight,3) }}</td>
            <td style="vertical-align: center;border: 1px solid #000000;" >{{ round((int)($ingredient->loaded_time /60),2).':'.$ingredient->loaded_time %60}}</td>
            <td style="vertical-align: center;border: 1px solid #000000;" >{{$ingredient->user}}</td>
        </tr>
        @endforeach
        <tr>
            <td style="" ></td>
            <td style="vertical-align: center;border: 1px solid #000000;background-color: #d4d4d4" >Всього:</td>
            <td style="{{$style}}border: 1px solid #000000;background-color: #bcbcbc" >{{ $value->plan_weight }}</td>
            <td style="{{$style}}border: 1px solid #000000;background-color: #bcbcbc" >{{ $value->fact_weight }}</td>
            <td style="{{$style}}border: 1px solid #000000;background-color: #bcbcbc" >{{ $value->fact_weight-$value->plan_weight }}</td>
            <td style="{{$style}}border: 1px solid #000000;background-color: #bcbcbc" >{{ $value->duration }}</td>
            <td></td>
        </tr>

    @endforeach
    </tbody>
</table>
