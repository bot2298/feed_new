<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('rate', function ($attribute, $value, $parameters, $validator) {
            $sum=0;
            foreach ($value as $item) {
                $sum+= $item['rate'];
            }
            if((int)$sum!=100)
                return false;
            else
                return true;
        },'Sum proportions.rate must be equal to 100');
    }
}
