<?php

namespace App\Rules;

use App\Models\Ingredients;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class ingredientCode1CRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!(Ingredients::where('code_1c', 'like', $value)->where('deleted_at', NULL)->get()->isEmpty())){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'unique';
    }
}
