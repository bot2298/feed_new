<?php

namespace App\Exports;

use App\Http\Resources\ReportsExportResource;
use App\Models\Timetables;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpParser\Node\Stmt\While_;

class ReportsExport implements FromView , ShouldAutoSize
{
    private $request;
    private $template;
    private $data;
    private $second;

    public function __construct($template, $primary, $secondary){
        $this->template = $template;

        if(!is_null($secondary)){
            $this->data = $primary;
            $this->second = $secondary;
        }
        else{
            $this->data = $primary;
        }
    }

    public function view(): View
    {
        if(!is_null($this->second)) {
            return view($this->template, [
                'data' => collect(json_decode($this->data->toJson())),
                'second' => collect(json_decode($this->second->toJson())),
            ]);
        }else{
            return view($this->template, [
                'data' => collect(json_decode($this->data->toJson())),
            ]);
        }
    }


    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}
