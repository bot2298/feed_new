<?php

namespace App\Console\Commands;

use App\Models\Silages;
use Illuminate\Console\Command;

class silages_clearing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyday:silages_clearing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Silages::all() as $silage)
            $silage->update([
                'planned_weight' => 0
            ]);
        return 'okey';
    }
}
