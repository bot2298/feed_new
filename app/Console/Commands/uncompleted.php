<?php

namespace App\Console\Commands;

use App\Models\Timetables;
use Carbon\Carbon;
use Illuminate\Console\Command;

class uncompleted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyday:uncompleted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Timetables that is not done, will change status uncompleted';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = Timetables::whereDate('planned_at', '<', Carbon::now())->where('status', '!=', 'done')->pluck('id');
        foreach ($id as $a){
            $time = Timetables::findOrFail($a);
            $planned = Carbon::create($time->planned_at);
            $time->update([
                'status' => 'uncompleted',
                'planned_at' => $planned
            ]);
        }
        return 'okey';
    }
}
