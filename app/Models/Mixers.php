<?php

namespace App\Models;

use App\Scopes\FarmsScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mixers extends Model
{
    use SoftDeletes;

    protected $hidden = ['deleted_at'];

    protected $fillable = [
        'id',
        'farm_id',
        'name',
        'min_weight',
        'max_weight',
//        'mixing_time',
        'calibration_id',
        'weight_up',
        'rounding',
        'auto_load'
    ];

    protected $table = 'mixers';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }
}
