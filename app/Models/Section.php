<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    use SoftDeletes;

    protected $hidden = ['deleted_at'];

    protected $fillable = [
        'id',
        'sections_key',
        'section_name'
    ];

    protected $table = 'sections';
}
