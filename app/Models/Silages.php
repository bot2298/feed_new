<?php

namespace App\Models;

use App\Scopes\FarmsScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed id
 * @property string housing
 * @property string number
 * @property float max_weight
 * @property float net_weight
 * @property float planned_weight
 * @property string comment
 */
class Silages extends Model
{
    use SoftDeletes;

    /**
     * @var bool
     */
    public static $withoutAppends = false;

    protected $hidden = ['deleted_at'];

    protected $appends = ['housing_name'];

    protected $fillable = [
        'id',
        'farm_id',
        'housing_id',
        'number',
        'max_weight',
        'net_weight',
        'planned_weight',
        'comment'
    ];

    protected $table = 'silages';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }

    protected function getArrayableAppends()
    {
        if(self::$withoutAppends){
            return [];
        }
        return parent::getArrayableAppends();
    }

    public function getHousingNameAttribute(){

        return Housings::where('id', self::getAttribute('housing_id'))->withTrashed()->value('name');
    }
}
