<?php

namespace App\Models;

use App\Http\Resources\RationResource;
use App\Http\Resources\RecipesResource;
use App\Http\Resources\SilagesResource;
use App\Scopes\FarmsScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timetables extends Model
{
    use SoftDeletes;

    const STATUS_EMPTY = "empty";
    const STATUS_IN_PROGRESS = "in_progress";
    const STATUS_WAIT = "wait";
    const STATUS_DONE = "done";
    const STATUS_UNCOMPLETED = "uncompleted";
    const STATUS_CANCEL = "cancel";

    /**
     * @var bool
     */
    public static $withoutAppends = true;

    protected $hidden = ['deleted_at'];

    protected $casts = [
        'silages_ids' => 'array',
    ];

    protected $appends = array('recipe','silages_list','housing_list','ration');

    protected $fillable = [
        'id',
        'farm_id',
        'name',
        'planned_at',
        'recipe_id',
        'silages_ids',
        'change',
        'status',
        'user_id',
        'mixing_time',
        'download'
    ];

    protected $table = 'timetables';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }

    public function getRecipeAttribute(){

        return new RecipesResource(Recipes::withTrashed()->find(self::getAttribute('recipe_id')));
    }

    public function getRationAttribute(){
        $rat_id = Recipes::withTrashed()->find(self::getAttribute('recipe_id'))->ration_id;
        return new RationResource(Ration::find($rat_id));
    }

    public function getSilagesListAttribute(){

        $ids = self::getAttribute('silages_ids');

        Silages::$withoutAppends = true;

        return Silages::whereIn('id', $ids)->withTrashed()->get(["id","number","net_weight","planned_weight"])->toArray();
    }

    public function getHousingListAttribute(){

        $ids = self::getAttribute('silages_ids');

        Silages::$withoutAppends = false;

        return Silages::whereIn('id', $ids)->withTrashed()->get(["housing_id"])->toArray();
    }
}
