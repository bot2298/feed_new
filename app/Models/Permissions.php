<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permissions extends Model
{
    use SoftDeletes;


    protected $hidden = ['deleted_at'];

    protected $appends = ['section_name','section_key','user_name'];

    protected $fillable = [
        'id',
        'user_id',
        'section_id',
        'read',
        'write',
        'change',
        'delete'
    ];

    protected $table = 'permissions';

    public function getSectionNameAttribute()
    {
        return Section::where('id',self::getAttribute("section_id"))->withTrashed()->value('section_name');
    }
    public function getSectionKeyAttribute()
    {
        return Section::where('id',self::getAttribute("section_id"))->withTrashed()->value('sections_key');
    }
    public function getUserNameAttribute()
    {
        return User::where('id',self::getAttribute("user_id"))->withTrashed()->first()->getAttribute('name');
    }
}
