<?php

namespace App\Models;

use App\Scopes\FarmsScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Ingredients extends Model
{
    use SoftDeletes;

    protected $hidden = ['deleted_at'];

    protected $appends =[
        'weight_per_day',
        'remainder_day',
        'remainder_weight',
        'remainder_bags',
        'cost'
    ];

    protected $fillable = [
        'id',
        'farm_id',
        'name',
        'qr_code',
        'autoload',
        'weight_left',
        'code_1c'
    ];

    protected $table = 'ingredients';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }

    public function getRemainderWeightAttribute(){
//        if( Accounting::where('type',Accounting::TYPE_RETENTIONS)
//                ->where('ingredient_id', self::getAttribute('id'))
//                ->pluck('weight')->sum()
//            - Accounting::where('type',Accounting::TYPE_WRITE_OFF)
//                ->where('ingredient_id', self::getAttribute('id'))
//                ->pluck('weight')->sum() < 0)
//                return 0;
//        else
//            return  Accounting::where('type',Accounting::TYPE_RETENTIONS)
//                    ->where('ingredient_id', self::getAttribute('id'))
//                    ->pluck('weight')->sum()
//                - Accounting::where('type',Accounting::TYPE_WRITE_OFF)
//                    ->where('ingredient_id', self::getAttribute('id'))
//                    ->pluck('weight')->sum();
        return self::getAttribute('weight_left');
    }

    public function getRemainderBagsAttribute(){
        return Accounting::where('type',Accounting::TYPE_RETENTIONS)
                ->where('ingredient_id', self::getAttribute('id'))
                ->pluck('bags')->sum()
            - Accounting::where('type',Accounting::TYPE_WRITE_OFF)
                ->where('ingredient_id', self::getAttribute('id'))
                ->pluck('bags')->sum();
    }

    public function getCostAttribute(){
        $accounting = Accounting::where('type',Accounting::TYPE_RETENTIONS)
            ->where('ingredient_id', self::getAttribute('id'))
            ->latest()->first(['price','weight']);
        if (!empty($accounting))
            if($accounting->weight != 0)
                $cost = $accounting->price/$accounting->weight;
            else
                $cost = 0;

        return isset($cost)?($cost*self::getRemainderWeightAttribute()): 0;
    }

    public function getWeightPerDayAttribute(){
        $timetables = Timetables::whereDate('planned_at', Carbon::now())->pluck('id');
        $batches = Batches::whereIn('timetable_id', $timetables)->pluck('id');
        return Downloads::whereIn('batch_id', $batches)->where('ingredient_id', self::getAttribute('id'))->sum('weight');
//        return Accounting::where('type',Accounting::TYPE_WRITE_OFF)
//            ->whereDate('created_at', Carbon::today())
//            ->where('provider_id', 1)
//            ->where('ingredient_id', self::getAttribute('id'))
//            ->sum('weight');
    }

    public function getRemainderDayAttribute(){
        if(self::getWeightPerDayAttribute() != 0)
            return self::getRemainderWeightAttribute()/self::getWeightPerDayAttribute();
        else
            return 0;
    }
}
