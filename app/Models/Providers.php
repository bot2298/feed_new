<?php

namespace App\Models;

use App\Scopes\FarmsScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Providers extends Model
{
    use SoftDeletes;

    const TYPE_PROVIDER = 'provider';
    const TYPE_BUYER = 'buyer';
    const TYPE_SELF = 'self';

    const TYPE = [
        self::TYPE_PROVIDER =>  'Поставщик',
        self::TYPE_BUYER    =>  'Покупець',
        self::TYPE_SELF    =>  'Ферма'
    ];

    protected $hidden = ['deleted_at'];

    protected $fillable = [
        'id',
        'farm_id',
        'name',
        'type',
        'info',
    ];

    protected $table = 'providers';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }
}

