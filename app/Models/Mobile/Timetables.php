<?php

namespace App\Models\Mobile;

use App\Http\Resources\Mobile\ProportionsResource;
use App\Http\Resources\RecipesResource;
use App\Http\Resources\SilagesResource;
use App\Models\Proportions;
use App\Models\Recipes;
use App\Models\Silages;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timetables extends Model
{
    use SoftDeletes;

    const STATUS_EMPTY = "empty";
    const STATUS_IN_PROGRESS = "in_progress";
    const STATUS_WAIT = "wait";
    const STATUS_DONE = "done";

    /**
     * @var bool
     */
    public static $withoutAppends = true;

    protected $hidden = ['deleted_at'];

    protected $casts = [
        'silages_ids' => 'array',
    ];

    protected $appends = array('recipe','silages_list','housing_list','schedule_ingredients');

    protected $fillable = [
        'id',
        'name',
        'planned_at',
        'started_at',
        'ended_at',
        'recipe_id',
        'silages_ids',
        'change',
        'status',
        'download'
    ];

    protected $table = 'timetables';

    public function getRecipeAttribute(){

        return new RecipesResource(Recipes::withTrashed()->find(self::getAttribute('recipe_id')));
    }

    public function getScheduleIngredientsAttribute(){

        return ProportionsResource::collection(Proportions::where('recipes_id',self::getAttribute('recipe_id'))->get());
    }

    public function getSilagesListAttribute(){

        $ids = self::getAttribute('silages_ids');

        Silages::$withoutAppends = true;

        return Silages::whereIn('id', $ids)->withTrashed()->get(["id","number","net_weight","planned_weight"])->toArray();
    }

    public function getHousingListAttribute(){

        $ids = self::getAttribute('silages_ids');

        Silages::$withoutAppends = false;

        return Silages::whereIn('id', $ids)->withTrashed()->get(["housing_id"])->toArray();
    }


}
