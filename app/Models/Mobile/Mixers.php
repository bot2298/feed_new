<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mixers extends Model
{
    use SoftDeletes;

    protected $hidden = ['deleted_at'];

    protected $fillable = [
        'id',
        'name',
        'min_weight',
        'max_weight',
//        'mixing_time',
        'farm_id',
        'calibration_id'
    ];

    protected $table = 'mixers';
}
