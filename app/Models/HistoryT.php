<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryT extends Model
{
    protected $table = 'hist_timetables';
//    public $timestamps = false;
    protected $fillable = [
        'timetable_id',
        'status'
    ];
}
