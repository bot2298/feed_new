<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int user_id
 * @property int timetable_id
 * @property mixed plan_weight
 *
 */
class Downloads extends Model
{
    protected $fillable = [
        'id',
        'batch_id',
        'ingredient_id',
        'time',
        'weight',
        'loaded_at'
    ];

    protected $table = 'downloads_ingredient';
}
