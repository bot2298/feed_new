<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed recipes_id
 * @property mixed ingredient_id
 * @property mixed rate
 */
class Proportions extends Model
{

    protected $appends = array('ingredient_name','fact_price');

    protected $fillable = [
        'id',
        'recipes_id',
        'ingredient_id',
        'rate'
    ];

    protected $table = 'proportions';

    public function getIngredientNameAttribute(){

        return Ingredients::withTrashed()->find(self::getAttribute('ingredient_id'))->getAttribute('name');
    }

    public function getFactPriceAttribute(){
        if (Ingredients::withTrashed()->find(self::getAttribute('ingredient_id'))->getAttributeValue('remainder_weight') !=0)
            return Ingredients::withTrashed()->find(self::getAttribute('ingredient_id'))->getAttributeValue('cost') /
            Ingredients::withTrashed()->find(self::getAttribute('ingredient_id'))->getAttributeValue('remainder_weight');
        else
            return 0;
    }

}
