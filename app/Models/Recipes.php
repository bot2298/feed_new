<?php

namespace App\Models;

use App\Http\Resources\ProportionsResource;
use App\Scopes\FarmsScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipes extends Model
{

    use SoftDeletes;

    protected $hidden = ['deleted_at'];

    protected $appends = array('proportions','fact_price');

    /**
     * @var bool
     */
    public static $withoutAppends = true;


    protected $fillable = [
        'id',
        'farm_id',
        'name',
        'status',
        'comment',
        'ration_id',
        'closed'
    ];

    protected $table = 'recipes';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }

    protected function getArrayableAppends()
    {
        if(self::$withoutAppends){
            return [];
        }
        return parent::getArrayableAppends();
    }

    public function getProportionsAttribute(){

        return ProportionsResource::collection(Proportions::where('recipes_id',self::getAttribute('id'))->get());
    }

    public function getFactPriceAttribute(){
        $fact_price = 0;
        Proportions::where('recipes_id',self::getAttribute('id'))->get()->each(
            function ($item) use ($fact_price){
                $fact_price += $item->fact_price;
        });
        return $fact_price;
    }

}
