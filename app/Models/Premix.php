<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Premix extends Model
{
    protected $fillable = [
        'timetable_id',
        'ingredients_id',
        'need_premix',
        'need_oil'
    ];

    protected $table = 'premix';
}
