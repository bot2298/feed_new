<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Calibrations extends Model
{
    use SoftDeletes;

    protected $hidden = ['deleted_at'];

    protected $fillable = [
        'id',
        'mixer_id',
        'calibration',
        'created_at',
        'updated_at'
    ];

    protected $table = 'calibrations';

}
