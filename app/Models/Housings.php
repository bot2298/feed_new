<?php

namespace App\Models;

use App\Scopes\FarmsScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Housings extends Model
{
    use SoftDeletes;

    protected $hidden = ['deleted_at'];

    protected $appends = ['silages_list'];

    protected $fillable = [
        'id',
        'name',
        'farm_id',
        'comment'
    ];

    protected $table = 'housings';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }

    public function getSilagesListAttribute(){

        return Silages::where('housing_id', '=', self::getAttribute('id'))->withTrashed()->get(['id','number']);
    }
}
