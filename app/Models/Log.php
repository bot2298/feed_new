<?php

namespace App\Models;

use App\Scopes\FarmsScope;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    const TYPE =[
        'store'=>'Створення',
        'update'=>'Оновлення',
        'destroy'=>'Видалення'
    ];

    protected $fillable = [
        'id',
        'farm_id',
        'user_id',
        'resource_id',
        'tab',
        'type',
    ];

    protected $table = 'logs';

    protected $appends =[
        'user_name',
        'user_type_key',
        'user_type_name',
        'type_full'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }

    public function getUserNameAttribute()
    {
        return User::withTrashed()->find(self::getAttribute('user_id'))->getAttribute('first_name').' '.
            User::withTrashed()->find(self::getAttribute('user_id'))->getAttribute('last_name');
    }

    public function getUserTypeKeyAttribute()
    {
        return User::withTrashed()->find(self::getAttribute('user_id'))->getAttribute('group');
    }

    public function getUserTypeNameAttribute()
    {
        return User::withTrashed()->find(self::getAttribute('user_id'))->getAttribute('group');
    }

    public function getTypeFullAttribute()
    {
        return self::TYPE[self::getAttribute('type')];
    }
}
