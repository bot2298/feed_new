<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ration extends Model
{
    protected $table = 'ration';

    protected $fillable = [
        'id',
        'name',
        'farm_id',
        'comment',
        'code_1c'
    ];

}
