<?php

namespace App\Models;

use App\Scopes\FarmsScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 *
 * @property mixed bags
 * @property mixed weight
 * @property mixed price
 */
class Accounting extends Model
{

    const TYPE_RETENTIONS = 'retentions';
    const TYPE_WRITE_OFF = 'write-off';

    const TYPE = [
        self::TYPE_RETENTIONS   =>  'Поставщик',
        self::TYPE_WRITE_OFF    =>  'Покупець'
    ];

    protected $appends = ['ingredient_name','provider_name'];

    protected $fillable = [
        'id',
        'ingredient_id',
        'weight',
        'bags',
        'price',
        'provider_id',
        'farm_id',
        'type'
    ];

    protected $table = 'accountings';
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new FarmsScope());
    }

    public function getIngredientNameAttribute(){

        return Ingredients::withTrashed()->find(self::getAttribute('ingredient_id'))->getAttribute('name');
    }
    public function getProviderNameAttribute(){

        return Providers::withTrashed()->find(self::getAttribute('provider_id'))->getAttribute('name');
    }
}
