<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoneProducts extends Model
{
    protected $table = 'done_product';

    protected $fillable = [
        'id',
        'name',
        'weight',
        'farm_id',
        'cost'
    ];
}
