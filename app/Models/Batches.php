<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int user_id
 * @property int timetable_id
 * @property mixed plan_weight
 *
 */
class Batches extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'mixer_id',
        'timetable_id',
        'loaded_at',
        'plan_weight',
        'created_at'
    ];

    protected $table = 'batches';

}
