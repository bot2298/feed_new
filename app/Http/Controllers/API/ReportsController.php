<?php

namespace App\Http\Controllers\API;

use App\Exports\ReportsExport;
use App\Http\Resources\ExportBatchesResource;
use App\Http\Resources\ExportDowloadDetailResource;
use App\Http\Resources\ExportOneCResource;
use App\Http\Resources\exportOnlyRationResource;
use App\Http\Resources\exportRationResource;
use App\Http\Resources\ExportTwoCResource;
use App\Http\Resources\ReportsExportResource;
use App\Http\Resources\ReportsResource;
use App\Http\Resources\PaginationCollection;
use App\Http\Resources\TimetablesResource;
use App\Models\Batches;
use App\Models\HistoryT;
use App\Models\Log;
use App\Models\Ration;
use App\Models\Recipes;
use App\Models\Silages;
use App\Models\Timetables;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $timetables = Timetables::latest();
        if ($request->get('date_from') || $request->get('date_to')){
            $date_from = new Carbon($request->get('date_from') ??'first day of January 2016');
            $date_to = new Carbon($request->get('date_to') ??'first day of January 2120');
            $timetables = $timetables->whereBetween('planned_at',[$date_from,$date_to->addHours(23)->addMinutes(59)])->get()->sortByDesc('download')->sortBy('status');
        }else{
            $timetables = $timetables->whereDate('planned_at', Carbon::now())->get()->sortByDesc('download')->sortBy('status');
        }
        if ($request->get('status')){
            $timetables = $timetables->filter(
                function ($value) use ($request){
                    return $value->status == $request->get('status');
                }
            );
        }
        if ($request->get('recipe_id')){
            $timetables = $timetables->filter(
                function ($value) use ($request){
                    return $value->recipe_id == $request->get('recipe_id');
                }
            );
        }

        return $this->sendResponse(new PaginationCollection(ReportsResource::collection($timetables)), 'Timetables retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'required|unique:timetables',
            'recipe_id'=>"required|exists:recipes,id,deleted_at,NULL",
            'silages_ids'=>'required|array',
            'planned_at'=>'required|date',
            'change'=>'boolean'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if (!in_array(0,Silages::whereIn('id',$input['silages_ids'])->pluck('planned_weight')->toArray()))
            $input['status'] = Timetables::STATUS_WAIT;

        $input['farm_id'] = Auth::user()->farm_id;
        $timetable = Timetables::create($input);

        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $timetable->id;
        $log->tab = 'timetable';
        $log->type = 'store';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new TimetablesResource($timetable), 'Timetable created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $timetable = Timetables::find($id);

        if (is_null($timetable)) {
            return $this->sendError('Timetable not found.');
        }

        return $this->sendResponse(new TimetablesResource($timetable), 'Timetable retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Timetables $timetable
     * @return JsonResponse
     */
    public function update(Request $request, Timetables $timetable)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'unique:timetables,name,'.$timetable->id,
            'recipe_id'=>"exists:recipes,id,deleted_at,NULL",
            'silages_ids'=>'array',
            'change'=>'boolean'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $timetable->update($input);
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $timetable->id;
        $log->tab = 'timetable';
        $log->type = 'update';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new TimetablesResource($timetable), 'Timetables updated successfully.');
    }

//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param Timetables $timetables
//     * @return JsonResponse
//     * @throws Exception
//     */
    public function destroy(Timetables $timetables)
    {


        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $timetables->id;
        $log->tab = 'timetable';
        $log->type = 'destroy';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        $timetables->delete();

        return $this->sendResponse([], 'Timetable deleted successfully.');
    }

    /**
     * @param Request $request
     */
    public function export(Request $request)
    {
        $timetables = Timetables::latest();

        if ($request->get('date_from') || $request->get('date_to')){
            $date_from = new Carbon($request->get('date_from') ??'first day of January 2016');
            $date_to = new Carbon($request->get('date_to') ??'first day of January 2120');
            $timetables = $timetables->whereBetween('planned_at',[$date_from,$date_to->addDays(1)])->get();
        }else{
            $timetables = $timetables->whereDate('planned_at', Carbon::now())->get();
}
        if ($request->get('status')){
            $timetables = $timetables->filter(
                function ($value) use ($request){
                    return $value->status == $request->get('status');
                }
            );
        }
        if ($request->get('recipe_id')){
            $timetables = $timetables->filter(
                function ($value) use ($request){
                    return $value->recipe_id == $request->get('recipe_id');
                }
            );
        }

        $collection = ReportsExportResource::collection($timetables);

        return Excel::download(
            new ReportsExport(
        'report',
                $collection, Null
            ), 'invoices.xlsx'
        );

//        return $this->sendResponse(new TimetablesCollection(ReportsResource::collection($timetables)), 'Timetables retrieved successfully.');

    }
    /**
     * @param Request $request
     */
    public function exportRecipes(Request $request)
    {
        $timetables = HistoryT::latest();
//        if ($request->get('time')){
//            $time = new Carbon($request->get('date_from'));
            $time = new Carbon('2021-06-22 14:15:58');
            $timetables = $timetables->whereTime('created_at', '<=',$time)->get()->unique('timetable_id');
//        }else{

//            return $this->sendError('time');
//        }
        Session::put('time', $time);
        $time_id = $timetables->pluck('timetable_id');

        $batches = Batches::latest()->whereIn('timetable_id', $time_id)->get();

        $collection = ExportBatchesResource::collection($batches);

        return Excel::download(
            new ReportsExport(
                'reportMixers',
                $collection, Null
            ), 'invoices.xlsx');


//        return $this->sendResponse(new TimetablesCollection(ReportsResource::collection($timetables)), 'Timetables retrieved successfully.');

    }

    public function exportRation(Request $request)
    {
        $timetables = Timetables::latest();

        if ($request->get('date_from') || $request->get('date_to')) {
            $date_from = new Carbon($request->get('date_from') ?? 'first day of January 2016');
            $date_to = new Carbon($request->get('date_to') ?? 'first day of January 2120');
            $timetables = $timetables->whereBetween('planned_at', [$date_from, $date_to->addDays(1)])->get();
        } else {
//            $timetables = $timetables->whereDate('planned_at', Carbon::now())->get();
            $timetables = $timetables->get();
        }
        if ($request->get('status')) {
            $timetables = $timetables->filter(
                function ($value) use ($request) {
                    return $value->status == $request->get('status');
                }
            );
        }
        if ($request->get('recipe_id')) {
            $timetables = $timetables->filter(
                function ($value) use ($request) {
                    return $value->recipe_id == $request->get('recipe_id');
                }
            );
        }

        $rat_id = Recipes::whereIn('id', $timetables->pluck('recipe_id'))->pluck('ration_id');
        $ration = Ration::find($rat_id);
        $date1 = exportRationResource::collection($timetables);
        $date2 = exportOnlyRationResource::collection($ration);
        return Excel::download(
            new ReportsExport(
                'reportRation',
                $date1, $date2
            ), 'invoices.xlsx'
        );
    }
    public function exportOneC(Request $request, $value)
    {
        $timetable = Timetables::latest()->whereIn('status', ['in_progress', 'done'])->whereDate('planned_at', date($value))->get();

 //       var_dump($timetable);

        $collection = ExportOneCResource::collection($timetable);

        return Excel::download(
            new ReportsExport(
                'reportOneC',
                $collection, Null
            ), 'fact_1_C.xlsx');

//      return $this->sendResponse(new TimetablesCollection(ReportsResource::collection($timetables)), 'Timetables retrieved successfully.');
    }

    public function exportTwoC(Request $request)
    {
        $recipe = Recipes::get();

        //       var_dump($timetable);

        $collection = ExportTwoCResource::collection($recipe);

        return Excel::download(
            new ReportsExport(
                'reportTwoC',
                $collection, Null
            ), 'plan_1_C.xlsx');

//      return $this->sendResponse(new TimetablesCollection(ReportsResource::collection($timetables)), 'Timetables retrieved successfully.');
    }
}
