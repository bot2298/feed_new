<?php

namespace App\Http\Controllers\API;

use App\Models\Housings;
use App\Models\Ingredients;
use App\Models\Providers;
use App\Models\Recipes;
use App\Models\Silages;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ListsController extends BaseController
{
    /**
     * @param string $value
     * @param Request $request
     * @return JsonResponse
     */
    public function index($value, Request $request)
    {
        switch ($value){
            case 'ingredients':
                $result = Ingredients::all('id','name','qr_code')->each(function($row)
                {
                    $row->qr_code =env("APP_URL").$row->qr_code;
                    $row->setHidden(['cost','weight_per_day','remainder_day','remainder_weight','remainder_bags']);
                });
                break;
            case 'housings':
                $result = Housings::all('id','name')->each(function($row)
                {
                    $row->setHidden(['silages_list']);
                });
                break;
            case 'silages':
                $result = Silages::all('id','number')->each(function($row)
                {
                    $row->setHidden(['housing_name']);
                });
                break;
            case 'providers':
                $result = Providers::where('type', Providers::TYPE_PROVIDER)->get(['id','name']);
                break;
            case 'buyers':
                $result = Providers::where('type', Providers::TYPE_BUYER)->get(['id','name']);
                break;
            case 'recipes':

                if ($request->get('status')){
                    $status = $request->get('status');
                    $result = Recipes::where('status',filter_var($status, FILTER_VALIDATE_BOOLEAN))->get(['id','name','ration_id']);
                }
                else
                    $result = Recipes::all('id','name','ration_id');
                break;
            case 'groups':
                $result = User::GROUPS;
                break;
            default:
                return $this->sendError('parameter not fount','ingredients or housings or silages or providers or recipes or groups' );

        }

        return $this->sendResponse($result, 'List successfully.');

    }
}
