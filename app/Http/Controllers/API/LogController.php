<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PaginationCollection;
use App\Models\Log;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LogController extends BaseController
{
    /**
     * @param string $value
     * @param Request $request
     * @return JsonResponse
     */
    public function index($value, Request $request)
    {
        $logs = Log::where('tab',$value)->get();

        if ($request->get('date')){
            $logs = $logs->filter(
                function ($value) use ($request){
                    return $value->created_at->format('YYYY-MM-DD') == Carbon::parse($request->get('date') )->format('YYYY-MM-DD');
                }
            );
        }
        if ($request->get('user_type_key')){
            $logs = $logs->filter(
                function ($value) use ($request){
                    return $value->user_type_key == $request->get('user_type_key') ;
                }
            );
        }
        if ($request->get('user_id')){
            $logs = $logs->filter(
                function ($value) use ($request){
                    return $value->user_id == $request->get('user_id') ;
                }
            );
        }
        if ($request->get('type')){
            $logs = $logs->filter(
                function ($value) use ($request){
                    return $value->user_type_key == $request->get('type') ;
                }
            );
        }

        return $this->sendResponse(new PaginationCollection($logs), 'Logs successfully.');

    }
}

