<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PaginationCollection;
use App\Http\Resources\ProvidersResource;
use App\Models\Providers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;

class ProvidersController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->get('name')){
            $providers = Providers::where('type','!=',Providers::TYPE_SELF)->where('name','like',$request->get('name')."%");
        }
        else
            $providers = Providers::where('type','!=',Providers::TYPE_SELF);

        return $this->sendResponse(new PaginationCollection(ProvidersResource::collection($providers->latest()->get())), 'Providers retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'required|unique:providers,deleted_at,NULL|max:255',
            'type'=>'required|in:buyer,provider',
            'info'=>'max:255'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input['farm_id'] = Auth::user()->farm_id;

        $provider = Providers::create($input);

        return $this->sendResponse(new ProvidersResource($provider), 'Provider created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $providers = Providers::find($id);

        if (is_null($providers)) {
            return $this->sendError('Providers not found.');
        }

        return $this->sendResponse(new ProvidersResource($providers), 'Provider retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Providers $provider
     * @return JsonResponse
     */
    public function update(Request $request, Providers $provider)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'unique:providers,name,'.$provider->id.'|max:255',
            'type'=>'in:buyer,provider',
            'info'=>'max:255'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $provider->update($input);

        return $this->sendResponse(new ProvidersResource($provider), 'Provider updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Providers $provider
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Providers $provider)
    {
        $provider->delete();

        return $this->sendResponse([], 'Provider deleted successfully.');
    }
}
