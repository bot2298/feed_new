<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DoneProductResource;
use App\Http\Resources\PaginationCollection;
use App\Models\DoneProducts;
use Illuminate\Http\Request;

class DoneProductController extends BaseController
{
    public function index(Request $request)
    {
        $doneProd = DoneProducts::get();
        return $this->sendResponse(new PaginationCollection(DoneProductResource::collection($doneProd)), 'Done products retrieved successfully.');
    }

}
