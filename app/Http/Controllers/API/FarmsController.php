<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\HousingsResource;
use App\Http\Resources\PaginationCollection;
use App\Models\Farms;
use App\Models\Housings;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;

class FarmsController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $farms = Farms::all();
        return $this->sendResponse($farms, 'Farm created successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'required|unique:Farms',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $farms = Farms::create($input);

        return $this->sendResponse($farms, 'Farms created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $housing = Housings::find($id);

        if (is_null($housing)) {
            return $this->sendError('Housing not found.');
        }

        return $this->sendResponse(new HousingsResource($housing), 'Housing retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Housings $housing
     * @return JsonResponse
     */
    public function update(Request $request, Housings $housing)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'unique:housings,name,'.$housing->id
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $housing->update($input);

        return $this->sendResponse(new HousingsResource($housing), 'Housing updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Housings $housing
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Housings $housing)
    {

        if ($housing->getAttributeValue('silages_list')->isEmpty()){
            $housing->delete();
            return $this->sendResponse([], 'Housing deleted successfully.');
        }
        else
            return $this->sendError('Validation Error.', 'Housing has silages');
    }
}
