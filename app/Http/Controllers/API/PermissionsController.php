<?php

namespace App\Http\Controllers\API;

use App\Helpers\CollectionHelper;
use App\Http\Resources\PermissionCollection;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\SectionResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\PaginationCollection;
use App\Http\Resources\UsersPermissionResourse;
use App\Models\Permissions;
use App\Models\Section;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;

class PermissionsController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $sections = Section::all();
        return $this->sendResponse($sections, 'Sections retrieved successfully.');

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id'=>'required|exists:users,id',
            'section_id'=>'required|exists:sections,id',
            'permission.read'=>'required|boolean',
            'permission.write'=>'required|boolean',
            'permission.change'=>'required|boolean',
            'permission.delete'=>'required|boolean',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if (Permissions::where('user_id','=',$input['user_id'])->where( 'section_id','=',$input[ 'section_id'])->count() != 0){
            return $this->sendError('Validation Error.', 'Permission is created');
        }

        $permission = new Permissions();
        $permission->user_id = $input['user_id'];
        $permission->section_id = $input['section_id'];

        foreach ($input['permission'] as $key => $item){
            $permission->$key = $item;
        }
        $permission->save();

        return $this->sendResponse(new PermissionResource($permission), 'Permission created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $section = Permissions::where('section_id',$id)->whereIn('user_id', User::where('farm_id', Auth::user()->farm_id)->pluck('id'))->get();

        if (is_null($section)) {
            return $this->sendError('Permissions not found.');
        }

        return $this->sendResponse(new PaginationCollection(UsersPermissionResourse::collection($section)), 'Permission retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id'=>'required|exists:users,id',
            'section_id'=>'required|exists:sections,id',
            'permission.read'=>'boolean',
            'permission.write'=>'boolean',
            'permission.change'=>'boolean',
            'permission.delete'=>'boolean',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $permission = Permissions::where('user_id','=',$input['user_id'])->where( 'section_id','=',$input[ 'section_id'])->first();

        if (is_null($permission)) {
            return $this->sendError('Permissions not found.');
        }

        foreach ($input['permission'] as $key => $item){
            $permission->$key = $item;
        }

        $permission->update();

        return $this->sendResponse(new PermissionResource($permission), 'Permissions updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Permissions $permission
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Permissions $permission)
    {
        $permission->delete();

        return $this->sendResponse([], 'Permissions deleted successfully.');
    }

    public function parse(Request $request)
    {
        $sections = Section::withTrashed()->pluck('id');

        $users = User::withTrashed()->pluck('id');

        foreach ($sections as $section){

            foreach ($users as $user){
                if (Permissions::where('user_id','=',$user)->where( 'section_id','=',$section)->count() == 0){
                    $permission = new Permissions();
                    $permission->user_id = $user;
                    $permission->section_id = $section;
                    $permission->read = false;
                    $permission->write = false;
                    $permission->change = false;
                    $permission->delete = false;
                    $permission->save();
                }

            }
        }
        $keys = [
            'timetable',
            'recipes',
            'ingredients',
            'retentions',
            'write-off',
            'providers',
            'users',
            'logs',
            'reports',
            'silages',
            'mixers'
        ];

        foreach ($keys as $key =>$value){
            Section::find($key+1)->update(["sections_key"=>$value]);
        }
        return true;
    }
}
