<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PaginationCollection;
use App\Http\Resources\RationResource;
use App\Models\Log;
use App\Models\Ration;
use App\Models\Timetables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RationController extends BaseController
{
    public function index(Request $request)
    {
        $ration = Ration::get();
        if($request->get('code_1c')){
            $ration = $ration->filter(
                function ($value) use ($request){
                    return $value->code_1c == $request->get('code_1c');
                }
            );
        }
        return $this->sendResponse(new PaginationCollection(RationResource::collection($ration)), 'Ration retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'   => 'required|unique:ration',
            'code_1c'=> 'unique:ration'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input['farm_id'] = Auth::user()->farm_id;

        $ration = Ration::create($input);

        $ration = Ration::find($ration->id);

        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $ration->id;
        $log->tab = 'ration';
        $log->type = 'store';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new RationResource($ration), 'Ration created successfully.');
    }

    public function show($id)
    {
        $ration = Ration::find($id);

        if (is_null($ration)) {
            return $this->sendError('Ration not found.');
        }

        return $this->sendResponse(new RationResource($ration), 'Ration retrieved successfully.');
    }

    public function update(Request $request, Ration $ration)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'   => Rule::unique('ration')->ignore($ration->id),
            'code_1c'=> Rule::unique('ration')->ignore($ration->id)
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $ration->update($input);

        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $ration->id;
        $log->tab = 'ration';
        $log->type = 'update';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new RationResource($ration), 'Recipes updated successfully.');
    }

    public function destroy(Ration $ration)
    {
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $ration->id;
        $log->tab = 'ration';
        $log->type = 'delete';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();
        $ration->delete();

        return $this->sendResponse([], 'Ration deleted successfully.');
    }
}
