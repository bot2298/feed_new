<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PaginationCollection;
use App\Http\Resources\RecipesResource;
use App\Models\Accounting;
use App\Models\Ingredients;
use App\Models\Log;
use App\Models\Proportions;
use App\Models\Ration;
use App\Models\Recipes;
use App\Models\Timetables;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;

class RecipesController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */

    public function index(Request $request)
    {
        if ($request->get('in_timetable')){
            $ids = Timetables::groupBy('recipe_id')->pluck('recipe_id');
            if (filter_var($request->get('in_timetable'),FILTER_VALIDATE_BOOLEAN)){
                $recipes = Recipes::whereIn('id',$ids)->where('status', 1)->latest();
            }else{
                $recipes = Recipes::whereNotIn('id',$ids)->latest();
            }
        }
        else{
            $recipes = Recipes::latest();
        }
        if ($request->get('date_from') || $request->get('date_to')){
            $date_from = new Carbon($request->get('date_from') ??'first day of January 2016');
            $date_to = new Carbon($request->get('date_to') ??'first day of January 2120');
            $recipes = $recipes->whereBetween('created_at',[$date_from,$date_to->addDays(1)]);
        }
        if($request->get('name')){
            $recipes = $recipes->where('name', 'like', '%' . $request->get('name') . '%');
        }
        if($request->get('ration_id')){
            $recipes = $recipes->where('ration_id', $request->get('ration_id'));
        }

        $recipes = $recipes->get();
//        if ($request->get('date')){
//            $recipes = $recipes->filter(
//                function ($value) use ($request){
//                    return $value->created_at->format('YYYY-MM-DD') == Carbon::parse($request->get('date') )->format('YYYY-MM-DD');
//                }
//            );
//        }

//        if ($request->get('cost_from')){
//            $recipes = $recipes->filter(
//                function ($value) use ($request){
//                    return $value->fact_price >= $request->get('cost_from');
//                }
//            );
//        }
//        if ($request->get('cost_to')){
//            $recipes = $recipes->filter(
//                function ($value) use ($request){
//                    return $value->fact_price <= $request->get('cost_to');
//                }
//            );
//        }
        if ($request->get('status')){
            $recipes = $recipes->filter(
                function ($value) use ($request){
                    return boolval($value->status) == filter_var($request->get('status'),FILTER_VALIDATE_BOOLEAN);
                }
            );
        }
        return $this->sendResponse(new PaginationCollection(RecipesResource::collection($recipes)), 'Recipes retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name'=>'required|unique:recipes',
            'ration_id'=>'required',
            'proportions.*.ingredient_id'=>'required|exists:ingredients,id,deleted_at,NULL',
            'proportions'=>'rate',
            'status'=>'boolean'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input['farm_id'] = Auth::user()->farm_id;
        $recipe = Recipes::create($input);

        foreach ($input['proportions'] as $item){
            $proportion = new Proportions();
            $proportion->recipes_id = $recipe->id;
            $proportion->ingredient_id = $item['ingredient_id'];
            $proportion->rate = $item['rate'];
            $proportion->save();
            unset($proportion);
        }

        $recipe = Recipes::find($recipe->id);

        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $recipe->id;
        $log->tab = 'recipe';
        $log->type = 'store';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new RecipesResource($recipe), 'Recipes created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $recipe = Recipes::find($id);

        if (is_null($recipe)) {
            return $this->sendError('Recipes not found.');
        }

        return $this->sendResponse(new RecipesResource($recipe), 'Recipes retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Recipes $recipe
     * @return JsonResponse
     */
    public function update(Request $request, Recipes $recipe)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'unique:recipes,name,'.$recipe->id,
            'proportions.*.ingredient_id'=>'required|exists:ingredients,id,deleted_at,NULL',
            'proportions'=>'rate',
            'status'=>'boolean'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

//        if($input['status']){
//            if($recipe->closed = true){
//                return $this->sendError('Validation Error.', ['recipe'=>['Can not active recipe, it is closed']]);
//            }
//        }

        $recipe->update($input);

        if ($request->get('proportions')){
            Proportions::where('recipes_id',$recipe->id)->delete();

            foreach ($input['proportions'] as $item){
                $proportion = new Proportions();
                $proportion->recipes_id = $recipe->id;
                $proportion->ingredient_id = $item['ingredient_id'];
                $proportion->rate = $item['rate'];
                $proportion->save();
                unset($proportion);
            }

            $recipe = Recipes::find($recipe->id);
        }


        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $recipe->id;
        $log->tab = 'recipe';
        $log->type = 'update';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new RecipesResource($recipe), 'Recipes updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Recipes $recipe
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Recipes $recipe)
    {
        if((Timetables::where('recipe_id',$recipe->id)->get()->isEmpty())){
            $log = new Log();
            $log->user_id = Auth::id();
            $log->resource_id = $recipe->id;
            $log->tab = 'recipe';
            $log->type = 'destroy';
            $log->farm_id = Auth::user()->farm_id;
            $log->save();
            $recipe->delete();
            return $this->sendResponse([], 'Recipe deleted successfully.');
        }
        else{
            return $this->sendError('Validation Error.', 'Schedules have recipe');
        }

    }
}
