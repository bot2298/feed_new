<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\Mobile\MixersResource;
use App\Http\Resources\PaginationCollection;
use App\Models\Calibrations;
use App\Models\Mobile\Mixers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;

class MixersController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $mixers = Mixers::all();
        return $this->sendResponse(new PaginationCollection(MixersResource::collection($mixers)), 'Mixers retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'required|unique:mixers',
            'min_weight'=>'required',
            'max_weight'=>'required',
            'calibration_at'=>'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $mixer = Mixers::create($input);

        return $this->sendResponse(new MixersResource($mixer), 'Mixers created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $mixer = Mixers::find($id);

        if (is_null($mixer)) {
            return $this->sendError('Mixers not found.');
        }

        return $this->sendResponse(new MixersResource($mixer), 'Mixers retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $mixer_id
     * @return JsonResponse
     */
    public function update(Request $request,$mixer_id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'calibration'=>'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $calibration = new Calibrations();
        $calibration->mixer_id = $mixer_id;
        $calibration->calibration = $request->get('calibration');
        $calibration->save();

        $mixer = Mixers::find($mixer_id);
        $mixer->calibration_id = $calibration->id;
        $mixer->update();

        return $this->sendResponse(new MixersResource($mixer), 'Mixers updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Mixers $mixer
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Mixers $mixer)
    {
        $mixer->delete();

        return $this->sendResponse([], 'Mixers deleted successfully.');
    }
}
