<?php

namespace App\Http\Controllers\API\Mobile;

use App\Http\Controllers\API\BaseController;
use App\Http\Resources\PaginationCollection;
use App\Http\Resources\Mobile\ProportionsResource;
use App\Http\Resources\Mobile\TimetablesResource;
use App\Models\Accounting;
use App\Models\Batches;
use App\Models\Calibrations;
use App\Models\DoneProducts;
use App\Models\Downloads;
use App\Models\HistoryT;
use App\Models\Ingredients;
use App\Models\Mixers;
use App\Models\Mobile\Timetables;
use App\Models\Proportions;
use App\Models\Ration;
use App\Models\Recipes;
use App\Models\Silages;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SchedulesController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mixer'=>'required|exists:mixers,id,deleted_at,NULL',
            'planned_on'=>'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $timetables = Timetables::whereDate('planned_at',$request->get('planned_on'))
            ->where('status','!=',Timetables::STATUS_EMPTY)->get();

        return $this->sendResponse(new PaginationCollection(TimetablesResource::collection($timetables)), 'Timetables retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function store($id, Request $request)
    {

        $request->request->add(['mixer'=>$request->get('schedule_mixer')['mixer']]);

        $mixer = Mixers::findOrFail($request->get('schedule_mixer')['mixer']);
        if (!$mixer->calibration_id)
            return $this->sendError('Відкалібруйте міксер');
        if ($request->get('plan_weight') < $mixer->min_weight)
            return $this->sendError('Замала маса замісу');

        $timetable = Timetables::findOrFail($id);
        $timetablePlanWeight = Silages::whereIn('id',$timetable->silages_ids)->sum('planned_weight');

        if($timetablePlanWeight < $request->get('plan_weight')){
            return $this->sendError('Виберіть меншу масу замішування');
        }
        $proportions = Proportions::where('recipes_id',$timetable->recipe_id)->get();
        $max_weight = 0;
        foreach ($proportions as $proportion){
            $ingredient = Ingredients::findOrFail($proportion->ingredient_id);
            $fact_weight = $request->get('plan_weight')*$proportion->rate/100;
            if ($ingredient->remainder_weight < $fact_weight){
                if($max_weight< $ingredient->remainder_weight*100/$proportion->rate)
                    $max_weight = $ingredient->remainder_weight*100/$proportion->rate;
            }

        }
        if ($max_weight){
            return $this->sendError('Не вистачає інгредієнтів, виберіть масу меншу за '.$max_weight);
        }

        if (!Batches::where('timetable_id', $id)->where('loaded_at', null)->get()->isEmpty()){
            return $this->sendError('Не завантажений останій заміс');
        }

        $timetable->started_at = Carbon::parse($request->get('started_at'));
        $timetable->status = Timetables::STATUS_IN_PROGRESS;
        $timetable->update();

        $batches = new Batches();
        $batches->user_id = Auth::id();
        $batches->timetable_id = $id;
        $batches->plan_weight = $request->get('plan_weight');
        $batches->save();

        $temp = array (
            array (
                'id' => $batches->id,
                'worker' => Auth::id(),
                'schedule_animalfeed' => $id,
                'schedule_mixer' =>
                    array (
                        'id' => $batches->id,
                        'mixer' => $mixer->id,
                        'min_weight' => $mixer->min_weight,
                        'max_weight' => $mixer->max_weight,
                        'calibration' => Calibrations::findOrFail($mixer->calibration_id)->getAttribute('calibration'),
                        'rounding' => true,
//                        'skip_time' => $mixer->mixing_time,
                        'up_down' => false,
                    ),
                'plan_weight' => $batches->plan_weight,
                'status' => 'in_process',
                'change' => boolval($timetable->change),
                'started_at' => $request->get('started_at'),
                'loaded_at' => NULL,
                'mixer_balance' => 0.0,
            ),
        );

        HistoryT::create([
            'timetable_id' => $id,
            'status' => $timetable->status,
        ]);

        return $this->sendResponse($temp, 'Timetable created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function show($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mixer'=>'required|exists:mixers,id,deleted_at,NULL',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $timetable = Timetables::find($id);

        if (is_null($timetable)) {
            return $this->sendError('Timetable not found.');
        }

        return $this->sendResponse(new TimetablesResource($timetable), 'Timetable retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     *@param int $id
     *@param int $batches_id
     *@param Request $request
     * @return JsonResponse
     */
    public function update($id, $batches_id, Request $request)
    {
        $timetable = Timetables::findOrFail($id);
        $batches = Batches::findOrFail($batches_id);
        if (!is_null($batches->loaded_at))
            return $this->sendError('Заміс вже завантажений');
        $batches->loaded_at = Carbon::now();
        $batches->save();

        $timetableFactWeight = Batches::where('timetable_id',$timetable->id)->whereNotNull('loaded_at')->sum('plan_weight');
        $timetablePlanWeight = Silages::whereIn('id',$timetable->silages_ids)->sum('planned_weight');

        $timetable->status = Timetables::STATUS_WAIT;
        $timetable->download = 1;

        $plan = $batches->plan_weight;

        if ($timetableFactWeight >= $timetablePlanWeight){
            $timetable->status = Timetables::STATUS_DONE;
            foreach (Silages::whereIn('id', $timetable->silages_ids)->get() as $sil) {
                $sil->update([
                    'planned_weight' => 0
                ]);
            }
        }else{
            foreach (Silages::whereIn('id', $timetable->silages_ids)->get() as $sil){
                if($sil->planned_weight >= $plan) {
                    $sil->update([
                        'planned_weight' => $sil->planned_weight - $plan
                    ]);
                    $plan = 0;
                }else{
                    $plan = $plan - $sil->planned_weight;
                    $sil->update([
                        'planned_weight' => 0
                    ]);
                }
            }
        }
        $timetable->save();

        $ingredients = $request->get('schedule_ingredients');
        $loadTime = 0;

        foreach ($ingredients as $ingredient){

            $recipes = Recipes::findOrFail($timetable->recipe_id);
            $done_prod = DoneProducts::find($recipes->ration_id);
            if(isset($done_prod)){
                $done_prod->update([
                    'weight' => $done_prod->weight + $ingredient['fact_weight']
                ]);
            }else{
                DoneProducts::create([
                    'id' => $recipes->ration_id,
                    'name' => $recipes->name,
                    'farm_id' => Auth::user()->farm_id,
                    'weight' => $ingredient['fact_weight'],
                    'cost' => 100
                ]);
            }

            $input = [];
            $input['ingredient_id'] = $ingredient["id"];
            $input['bugs'] = 0;
            $input['price'] = 100;
            $input['weight'] = $ingredient['fact_weight'];
            $input['type'] = Accounting::TYPE_WRITE_OFF;
            $input['farm_id'] = Auth::user()->farm_id;
            $input['provider_id'] = 1;
            $loadTime += $ingredient["time_load"];

            Accounting::create($input);

            $ingred = Ingredients::findOrFail($ingredient["id"]);
            $fact = $ingred->remainder_weight - $ingredient['fact_weight'];
            if($fact<0){
                $fact = 0;
            }
            $ingred->update([
                'weight_left'=> $fact
            ]);

            Downloads::create([
                'batch_id'=>$batches_id,
                'ingredient_id'=>$ingredient["id"],
                'time'=>$ingredient['time_load'],
                'weight'=>$ingredient['fact_weight'],
                'loaded_at'=>Carbon::create($ingredient['loaded_at'])
            ]);

        }
        $batches->time = $loadTime;
        $batches->update();

        Session::put('planned_weight', $batches->plan_weight);
        Session::put('bathes', $batches_id);

        $timetable = array (
                    'worker' => $batches->user_id ,
                    'schedule_animalfeed' => $id,
                    'schedule_mixer' => $batches_id,
                    'plan_weight' => $batches->plan_weight,
                    'started_at' => $timetable->started_at,
                    'loaded_at' => $batches->loaded_at,
                    'schedule_ingredients' => ProportionsResource::collection(Proportions::where('recipes_id',$timetable->recipe_id)->get()),
                    'detail' => '100',
        );

        HistoryT::create([
            'timetable_id' => $id,
            'status' => $timetable->status,
        ]);

        return $this->sendResponse($timetable, 'Timetables updated successfully.');
    }

    public function updating($id, $batch_id, Request $request){
        $ingredient = $request->all();
        $ids_down = Downloads::where('batch_id', $batch_id)->pluck('ingredient_id');
        $a = array();
        foreach ($ids_down as $ids){
            array_push($a, $ids);
        }
        if(in_array($ingredient["id"], $a))
            return $this->sendError('Ви вже завантажили цей інгрідіент');

        $timetable = Timetables::findOrFail($id);
        $batches = Batches::findOrFail($batch_id);

        $timetablePlanWeight = Silages::whereIn('id',$timetable->silages_ids)->sum('planned_weight');
        $timetable->status = Timetables::STATUS_IN_PROGRESS;
        $timetable->download = 1;

        $ingred = Ingredients::findOrFail($ingredient["id"]);
        $fact = $ingred->remainder_weight - $ingredient['fact_weight'];
        if($fact<0){
            $fact = 0;
        }
        $ingred->update([
            'weight_left'=> $fact
        ]);

        Downloads::create([
            'batch_id'=>$batch_id,
            'ingredient_id'=>$ingredient['id'],
            'time'=>$ingredient['time_load'],
            'weight'=>$ingredient['fact_weight'],
            'loaded_at'=>Carbon::create($ingredient['loaded_at'])
        ]);

        $recipes = Recipes::findOrFail($timetable->recipe_id);
        $ration_name = Ration::findOrFail($recipes->ration_id)->name;
        $done_prod = DoneProducts::find($recipes->ration_id);
        if(isset($done_prod)){
            $done_prod->update([
                'weight' => $done_prod->weight + $ingredient['fact_weight']
            ]);
        }else{
            DoneProducts::create([
                'id' => $recipes->ration_id,
                'name' => $ration_name,
                'farm_id' => Auth::user()->farm_id,
                'weight' => $ingredient['fact_weight'],
                'cost' => 100
            ]);
        }

        Session::put('planned_weight', $batches->plan_weight);
        Session::put('bathes', $batch_id);

        if(Downloads::where('batch_id', $batch_id)->count('ingredient_id') == Proportions::where('recipes_id', $timetable->recipe_id)->count('ingredient_id')){
            if ($timetablePlanWeight - $batches->plan_weight <= 0){
                $timetable->status = Timetables::STATUS_DONE;
                foreach (Silages::whereIn('id', $timetable->silages_ids)->get() as $sil) {
                    $sil->update([
                        'planned_weight' => 0
                    ]);
                }
            }
            else{
                $timetable->status = Timetables::STATUS_WAIT;
                $plan = $batches->plan_weight;
                foreach (Silages::whereIn('id', $timetable->silages_ids)->get() as $sil){
                    if($sil->planned_weight > $plan) {
                        $sil->update([
                            'planned_weight' => $sil->planned_weight - $plan
                        ]);
                    }else{
                        $plan = $plan - $sil->planned_weight;
                        $sil->update([
                            'planned_weight' => 0
                        ]);
                    }
                }
            }
            $batches->loaded_at = Carbon::now()->format('Y-m-d H:i:s');
            $batches->time = Downloads::where('batch_id', $batch_id)->sum('time');

            HistoryT::create([
                'timetable_id' => $id,
                'status' => $timetable->status,
            ]);

        }

        $timetable->update();
        $batches->update();


        $timet = array (
            'worker' => $batches->user_id ,
            'schedule_animalfeed' => $timetable->id,
            'schedule_mixer' => $batch_id,
            'plan_weight' => $batches->plan_weight,
            'started_at' => $timetable->started_at,
            'loaded_at' => $batches->loaded_at,
            'schedule_ingredients' => ProportionsResource::collection(Proportions::where('recipes_id',$timetable->recipe_id)->get()),
            'detail' => '100',
        );
        return $this->sendResponse($timet, 'Timetables updated successfully.');
    }

//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param Timetables $timetables
//     * @return JsonResponse
//     * @throws Exception
//     */
    public function destroy(Timetables $timetables)
    {
        $timetables->delete();

        return $this->sendResponse([], 'Timetable deleted successfully.');
    }
}
