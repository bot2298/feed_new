<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\MixersResource;
use App\Http\Resources\PaginationCollection;
use App\Http\Resources\UsersResource;
use App\Models\Log;
use App\Models\Mixers;
use App\Models\Permissions;
use App\Models\Section;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;

class UsersController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $users = User::where('farm_id', Auth::user()->farm_id)->get();
        return $this->sendResponse(new PaginationCollection(UsersResource::collection($users)), 'Users retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'first_name' =>'required',
            'last_name'=>'required',
            'group_key'=>'required',
            'phone'=>'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input = $request->all();
        $input['group'] = $input['group_key'];
        $input['name'] =  $input['first_name'].' '. $input['last_name'];
        unset($input['group_key']);
        $input['password'] = bcrypt($input['password']);
        $input['farm_id'] = Auth::user()->farm_id;
        $user = User::create($input);

        $sections = Section::withTrashed()->pluck('id');


        foreach ($sections as $section){

                $permission = new Permissions();
                $permission->user_id = $user->id;
                $permission->section_id = $section;
                $permission->read = false;
                $permission->write = false;
                $permission->change = false;
                $permission->delete = false;
                $permission->save();
        }
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $user->id;
        $log->tab = 'employee';
        $log->type = 'store';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new UsersResource($user), 'User register successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return $this->sendError('User not found.');
        }

        return $this->sendResponse(new UsersResource($user), 'User retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function update(Request $request, User $user)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'email|unique:users,email,'.$user->id,
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input = $request->all();

        if ($request->get('group_key')){
            $input['group'] = $input['group_key'];
            unset($input['group_key']);

        }
        if ($request->get('password')){
            $input['password'] = bcrypt($input['password']);
        }

        $user->update($input);
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $user->id;
        $log->tab = 'employee';
        $log->type = 'update';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new UsersResource($user), 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return JsonResponse
     * @throws Exception
     */

    public function destroy(User $user)
    {
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $user->id;
        $log->tab = 'employee';
        $log->type = 'delete';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        $user->delete();

        return $this->sendResponse([], 'User deleted successfully.');
    }
}
