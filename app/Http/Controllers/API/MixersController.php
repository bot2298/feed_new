<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\MixersResource;
use App\Http\Resources\PaginationCollection;
use App\Models\Mixers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;

class MixersController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $mixers = Mixers::all();
        return $this->sendResponse(new PaginationCollection(MixersResource::collection($mixers)), 'Mixers retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'required|unique:mixers',
            'min_weight'=>'required',
            'max_weight'=>'required',
            'auto_load'=>'numeric|min:0|max:10|nullable',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input['farm_id'] = Auth::user()->farm_id;
        $mixer = Mixers::create($input);

        return $this->sendResponse(new MixersResource($mixer), 'Mixers created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $mixer = Mixers::find($id);

        if (is_null($mixer)) {
            return $this->sendError('Mixers not found.');
        }

        return $this->sendResponse(new MixersResource($mixer), 'Mixers retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Mixers $mixer
     * @return JsonResponse
     */
    public function update(Request $request, Mixers $mixer)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'unique:mixers,name,'.$mixer->id
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $mixer->update($input);

        return $this->sendResponse(new MixersResource($mixer), 'Mixers updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Mixers $mixer
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Mixers $mixer)
    {
        $mixer->delete();

        return $this->sendResponse([], 'Mixers deleted successfully.');
    }
}
