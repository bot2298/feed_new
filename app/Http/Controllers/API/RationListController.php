<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\RationListResource;
use App\Models\Ration;
use Illuminate\Http\Request;

class RationListController extends BaseController
{
    public function index(Request $request)
    {
        $ration = Ration::get();
        return $this->sendResponse(RationListResource::collection($ration), 'Ration retrieved successfully.');
    }
}
