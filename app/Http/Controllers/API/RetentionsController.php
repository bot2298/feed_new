<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PaginationCollection;
use App\Http\Resources\AccountingResource;
use App\Models\Accounting;
use App\Models\Ingredients;
use App\Models\Log;
use App\Models\Providers;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;

class RetentionsController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $retentions = Accounting::where('type', Accounting::TYPE_RETENTIONS);

        if ($request->get('price_from') || $request->get('price_to')){
            $retentions = $retentions->whereBetween('price',[
                (float)$request->get('price_from')??$retentions->min('price'),
                (float)$request->get('price_to')??$retentions->max('price')
            ]);
        }
        if ($request->get('date_from') || $request->get('date_to')){
            $date_from = new Carbon($request->get('date_from') ??'first day of January 2016');
            $date_to = new Carbon($request->get('date_to') ??'first day of January 2120');
            $retentions = $retentions->whereBetween('created_at',[$date_from,$date_to]);
        }
        if ($request->get('weight_from') || $request->get('weight_to')){
            $retentions = $retentions->whereBetween('weight',[
                (float)$request->get('weight_from')??$retentions->min('weight'),
                (float)$request->get('weight_to')??$retentions->max('weight')
            ]);
        }
        if ($request->get('ingredient_ids')){
            $retentions = $retentions->whereIn('ingredient_id', explode(',',$request->get('ingredient_ids')));

        }
        if ($request->get('provider_ids')){
            $retentions = $retentions->whereIn('provider_id', explode(',',$request->get('provider_ids')));
        }

        return $this->sendResponse(new PaginationCollection(AccountingResource::collection($retentions->latest()->get())), 'Retentions retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'ingredient_id'=>'required|exists:ingredients,id,deleted_at,NULL',
            'provider_id'=>'required|exists:providers,id,deleted_at,NULL',
            'price'=>'numeric|min:0',
            'weight'=>'numeric|min:1',
            'bugs'=>'integer|min:0',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input['type'] = Accounting::TYPE_RETENTIONS;
        $input['farm_id'] = Auth::user()->farm_id;
        $retention = Accounting::create($input);

        $ingred = Ingredients::findOrFail($input['ingredient_id']);
        if($ingred->weight_left < 0)
            $ingred->weight_left = 0;
        if($input['weight'])
            $ingred->weight_left = $ingred->weight_left + $input['weight'];

        $ingred->save();

        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $retention->id;
        $log->tab = 'retention';
        $log->type = 'store';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new AccountingResource($retention), 'Retentions created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $retention = Accounting::find($id);

        if (is_null($retention)) {
            return $this->sendError('Retentions not found.');
        }

        return $this->sendResponse(new AccountingResource($retention), 'Retentions retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Accounting $retention
     * @return JsonResponse
     */
    public function update(Request $request, Accounting $retention)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'ingredient_id'=>'exists:ingredients,id,deleted_at,NULL',
            'provider_id'=>'exists:providers,id,deleted_at,NULL',
            'price'=>'numeric',
            'weight'=>'numeric',
            'bugs'=>'integer',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $retention->update($input);
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $retention->id;
        $log->tab = 'retention';
        $log->type = 'update';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new AccountingResource($retention), 'Retentions updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Accounting $accounting
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Accounting $accounting)
    {
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $accounting->id;
        $log->tab = 'retention';
        $log->type = 'delete';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();
        $accounting->delete();

        return $this->sendResponse([], 'Retention deleted successfully.');
    }
}
