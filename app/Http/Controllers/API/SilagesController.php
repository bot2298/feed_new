<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\Silage as SilageResource;
use App\Http\Resources\PaginationCollection;
use App\Http\Resources\SilagesCollection;
use App\Http\Resources\SilagesResource;
use App\Models\Housings;
use App\Models\Silage;
use App\Models\Silages;
use App\Models\Timetables;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SilagesController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->all()){
            $silages = Silages::query();

            if ($request->get('max_weight_from') || $request->get('max_weight_to')){
                $silages = $silages->whereBetween('max_weight',[
                    $request->get('max_weight_from')?(float)$request->get('max_weight_from'):$silages->min('max_weight'),
                    $request->get('max_weight_to')?(float)$request->get('max_weight_to'):$silages->max('max_weight')
                ]);
            }
            if ($request->get('net_weight_from') || $request->get('net_weight_to')){
                $silages = $silages->whereBetween('net_weight',[
                    $request->get('net_weight_from')?(float)$request->get('net_weight_from'):$silages->min('net_weight'),
                    $request->get('net_weight_to')?(float)$request->get('net_weight_to'):$silages->max('net_weight')
                ]);
            }
            if ($request->get('number')){
                $silages = $silages->where('number', 'like',"%".$request->get('number')."%");
            }
            if ($request->get('housing_ids')){

                $silages = $silages->whereIn('housing_id', explode(',',$request->get('housing_ids')));
            }

            $silages = $silages->latest()->get();
        }
        else{
            $silages = Silages::latest()->get();
        }

        return $this->sendResponse(new PaginationCollection(SilagesResource::collection($silages)), 'Silages retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'housing_id'=>'required|exists:housings,id,deleted_at,NULL',
            'number'=>'required|unique:silages,number',
            'max_weight'=>'required',
            'net_weight'=>'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $input['farm_id'] = Auth::user()->farm_id;
        $input['planned_weight'] = 0;

        $silage = Silages::create($input);

        return $this->sendResponse(new SilagesResource($silage), 'Silage created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $silage = Silages::find($id);

        if (is_null($silage)) {
            return $this->sendError('Silage not found.');
        }

        return $this->sendResponse(new SilagesResource($silage), 'Silage retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Silages $silage
     * @return JsonResponse
     */
    public function update(Request $request, Silages $silage)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'number'=>'unique:silages,number,'.$silage->id,
            'housing_id'=>'exists:housings,id,deleted_at,NULL',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $silage->update($input);

        return $this->sendResponse(new SilagesResource($silage), 'Silage updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Silages $silage
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Silages $silage)
    {
        $silagesInTimetable = array_unique(array_merge( Timetables::pluck('silages_ids')->toArray()));

        if (!in_array($silage->id, $silagesInTimetable)){
            $silage->delete();
            return $this->sendResponse([], 'Silages deleted successfully.');
        }
        else
            return $this->sendError('Validation Error.', 'Schedules have silages');
    }
}
