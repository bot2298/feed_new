<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PaginationCollection;
use App\Http\Resources\ReportsResource;
use App\Http\Resources\TimetablesResource;
use App\Models\Batches;
use App\Models\DoneProducts;
use App\Models\Downloads;
use App\Models\HistoryT;
use App\Models\Ingredients;
use App\Models\Log;
use App\Models\Premix;
use App\Models\Ration;
use App\Models\Recipes;
use App\Models\Silages;
use App\Models\Timetables;
use App\Models\Proportions;
use App\Models\Accounting;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use http\Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use function Matrix\add;

class TimetablesController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'planned_on'=>'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $timetables = Timetables::whereDate('planned_at',$request->get('planned_on'))->latest()->get()->sortByDesc('download')->sortBy('status');

        return $this->sendResponse(new PaginationCollection(ReportsResource::collection($timetables)), 'Timetables retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'ration_id'=>'required|exists:ration,id',
            'recipe_id'=>"required|exists:recipes,id,deleted_at,NULL",
            'silages_list'=>'required|array',
            'planned_at'=>'required|date',
            'mixing_time'=>'numeric|required',
            'change'=>'boolean'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input['name'] = Ration::find($input['ration_id'])->name;

        $ingreds_ids = Proportions::where('recipes_id', $request->get('recipe_id'))->pluck('ingredient_id');
        $timtabl = Timetables::whereDate('planned_at', Carbon::create($request->get('planned_at')))->where('deleted_at', NULL)->where('status','!=','done')->pluck('id');
        $input['silages_ids'] = array();
        foreach ($request->get("silages_list") as $silage){
            array_push($input['silages_ids'], $silage["id"]);
        }

        foreach ($input['silages_ids'] as $id){
            $t = Timetables::whereDate('planned_at', Carbon::create($request->get('planned_at')))->where('deleted_at', NULL)->where('status','!=','done')->whereJsonContains('silages_ids', $id)->pluck('silages_ids');
            foreach ($t as $T){
                foreach ($T as $l) {
                    if ([$id] == [$l])
                        return $this->sendError('Validation Error.', ["silages" => ["silages is full"]]);
                }
            }
        }
        foreach ($request->get("silages_list") as $silage){
            Silages::find($silage["id"])->update(["planned_weight" => $silage["weight"]]);
        }
        $input['status'] = Timetables::STATUS_WAIT;

        foreach($ingreds_ids as $id) {

            $plan_ingred_weight = 0;

            foreach ($timtabl as $table){
                $plan_weightes = Silages::whereIn('id', Timetables::where('id', $table)->pluck('silages_ids'))->sum('planned_weight');
                $plan_ingred_rate = Proportions::where('recipes_id', Timetables::where('id', $table)->pluck('recipe_id'))->where('ingredient_id', $id)->sum('rate');
                $plan_ingred_weight += $plan_weightes * $plan_ingred_rate/100;
            }

            $ingreds_weight = Ingredients::where('id', $id)->sum('weight_left');
            $ingreds_rate = Proportions::where('recipes_id', $request->get('recipe_id'))->where('ingredient_id', $id)->sum('rate');
            $plan_weight = Silages::whereIn('id', $input['silages_ids'])->sum('planned_weight');
            $ingr_plan_weight = ($plan_weight * $ingreds_rate / 100) + $plan_ingred_weight;

            if ($ingreds_weight < $ingr_plan_weight) {
                return $this->sendError('Validation Error.', ["ingredients_left" => ['no ingredients for planning']]);
            }
        }

        $input['farm_id'] = Auth::user()->farm_id;
        $input['user_id'] = Auth::id();
        $timetable = Timetables::create($input);

        foreach ($ingreds_ids as $id){
            Premix::create([
                'timetable_id' => $timetable->id,
                'ingredients_id' => $id
            ]);
        }

        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $timetable->id;
        $log->tab = 'timetable';
        $log->type = 'store';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        HistoryT::create([
            'timetable_id' => $timetable->id,
            'status' => $timetable->status,
        ]);

        return $this->sendResponse(new TimetablesResource($timetable), 'Timetable created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $timetable = Timetables::find($id);

        if (is_null($timetable)) {
            return $this->sendError('Timetable not found.');
        }

        return $this->sendResponse(new TimetablesResource($timetable), 'Timetable retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Timetables $timetable
     * @return JsonResponse
     */
    public function update(Request $request, Timetables $timetable)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'recipe_id'=>"exists:recipes,id,deleted_at,NULL",
            'silages_ids'=>'array',
            'change'=>'boolean'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if ($request->get("silages_list")){
            foreach ($request->get("silages_list") as $silage){
                Silages::find($silage["id"])->update(["planned_weight" => $silage["weight"]]);
            }
            $empty = Silages::whereIn('id',$timetable->silages_ids)
                ->where('planned_weight','=',0)
                ->get()
                ->isEmpty();
            if ($empty){
                $input['status'] = Timetables::STATUS_WAIT;
            }
            else{
                $input['status'] = Timetables::STATUS_EMPTY;
            }

        }

        if($request->get('ration_id')) {
            $input['name'] = Ration::findOrFail($input['ration_id'])->name;
        }

        $timetable->update($input);
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $timetable->id;
        $log->tab = 'timetable';
        $log->type = 'update';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new TimetablesResource(Timetables::findOrFail($timetable->id)), 'Timetables updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $timetables = Timetables::find($id);
        foreach (Silages::whereIn('id', $timetables->silages_ids)->get() as $sil){
            $sil->update([
                'planned_weight' => 0
            ]);
        }

        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $timetables->id;
        $log->tab = 'timetable';
        $log->type = 'destroy';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        $timetables->delete();

        return $this->sendResponse([], 'Timetable deleted successfully.');
    }

    public function premixUpdate(Request $request, $id){
        $input = $request->all();
        if(Timetables::find($id)->status != Timetables::STATUS_WAIT)
            return $this->sendError('Validation Error.', ["premix"=>["timetable has batches"]]);
        foreach ($input['ingred'] as $in){
            Premix::where('timetable_id', $id)->where('ingredients_id', $in["id"])->update([
                'need_premix' => $in['premix'],
                'need_oil' => $in['oil']
            ]);
        }
        $prem = Premix::where('timetable_id', $id)->get();
        return $this->sendResponse($prem, 'Premix update successfully.');
    }
}
