<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PaginationCollection;
use App\Http\Resources\AccountingResource;
use App\Models\Accounting;
use App\Models\Ingredients;
use App\Models\Providers;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;

class WriteOffController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $writeOff = Accounting::where('type', Accounting::TYPE_WRITE_OFF)->where('provider_id','!=',1);
        if ($request->all()){

            if ($request->get('price_from') || $request->get('price_to')){
                $writeOff = $writeOff->whereBetween('price',[
                    (float)$request->get('price_from')??$writeOff->min('price'),
                    (float)$request->get('price_to')??$writeOff->max('price')
                ]);
            }
            if ($request->get('date_from') || $request->get('date_to')){
                $date_from = new Carbon($request->get('date_from') ??'first day of January 2016');
                $date_to = new Carbon($request->get('date_to') ??'first day of January 2120');
                $writeOff = $writeOff->whereBetween('created_at',[$date_from,$date_to]);
            }
            if ($request->get('weight_from') || $request->get('weight_to')){
                $writeOff = $writeOff->whereBetween('weight',[
                    (float)$request->get('weight_from')??$writeOff->min('weight'),
                    (float)$request->get('weight_to')??$writeOff->max('weight')
                ]);
            }
            if ($request->get('ingredient_ids')){
                $writeOff = $writeOff->whereIn('ingredient_id', explode(',',$request->get('ingredient_ids')));
            }
            if ($request->get('provider_ids')){
                $writeOff = $writeOff->whereIn('provider_id', explode(',',$request->get('provider_ids')));
            }
        }


        return $this->sendResponse(new PaginationCollection(AccountingResource::collection($writeOff->latest()->get())), 'Write-off retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'ingredient_id'=>'required|exists:ingredients,id,deleted_at,NULL',
            'provider_id'=>'required|exists:providers,id,deleted_at,NULL',
            'price'=>'numeric|min:0',
            'weight'=>'numeric|min:1',
            'bugs'=>'integer',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }


        $ingredient = Ingredients::find($input['ingredient_id']);
        if ($ingredient->remainder_weight < $input['weight'])
            return $this->sendError('Not enough weight in storage');

        $input['type'] = Accounting::TYPE_WRITE_OFF;
        $input['farm_id'] = Auth::user()->farm_id;
        $ingredient->weight_left = $ingredient->remainder_weight - $input['weight'];
        if($ingredient->weight_left < 0)
            $ingredient->weight_left = 0;
        $ingredient->save();
        $writeOff = Accounting::create($input);



        return $this->sendResponse(new AccountingResource($writeOff), 'Write-off created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $writeOff = Accounting::find($id);

        if (is_null($writeOff)) {
            return $this->sendError('Retentions not found.');
        }

        return $this->sendResponse(new AccountingResource($writeOff), 'Write-off retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Accounting $writeOff
     * @return JsonResponse
     */
    public function update(Request $request, Accounting $writeOff)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'ingredient_id'=>'exists:ingredients,id,deleted_at,NULL',
            'provider_id'=>'exists:providers,id,deleted_at,NULL',
            'price'=>'numeric',
            'weight'=>'numeric',
            'bugs'=>'integer',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $writeOff->update($input);

        return $this->sendResponse(new AccountingResource($writeOff), 'Write-off updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Accounting $writeOff
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Accounting $writeOff)
    {
        $writeOff->delete();

        return $this->sendResponse([], 'Write-off deleted successfully.');
    }
}
