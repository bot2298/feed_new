<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PaginationCollection;
use App\Http\Resources\IngredientsResource;
use App\Models\Ingredients;
use App\Models\Log;
use App\Models\Proportions;
use App\Models\Recipes;
use App\Rules\ingredientCode1CRule;
use App\Rules\ingredientRule;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Validation\Rule;

class IngredientsController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $ingredients = Ingredients::all();
        if ($request->all()){
            if ($request->get('cost_from')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->cost >= $request->get('cost_from');
                    }
                );
            }
            if ($request->get('cost_to')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->cost <= $request->get('cost_to');
                    }
                );
            }
            if ($request->get('remainder_day_from')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->remainder_day >= $request->get('remainder_day_from');
                    }
                );
            }
            if ($request->get('remainder_day_to')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->remainder_day <= $request->get('remainder_day_to');
                    }
                );
            }
            if ($request->get('remainder_weight_from')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->remainder_weight >= $request->get('remainder_weight_from');
                    }
                );
            }
            if ($request->get('remainder_weight_to')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->remainder_weight <= $request->get('remainder_weight_to');
                    }
                );
            }
            if ($request->get('per_day_from')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->weight_per_day >= $request->get('per_day_from');
                    }
                );
            }
            if ($request->get('per_day_to')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->weight_per_day <= $request->get('per_day_to');
                    }
                );
            }
            if($request->get('code_1c')){
                $ingredients = $ingredients->filter(
                    function ($value) use ($request){
                        return $value->code_1c == $request->get('code_1c');
                    }
                );
            }

        }

        return $this->sendResponse(new PaginationCollection(IngredientsResource::collection($ingredients)), 'Ingredients retrieved successfully.');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => [
                'required',
                new ingredientRule($request)
                ],
            'code_1c'=> new IngredientCode1CRule($request),
            'autoload'=>'boolean'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input['farm_id'] = Auth::user()->farm_id;

        $ingredient = Ingredients::create($input);
//        $name = md5(time());
//        \QrCode::size(500)->format('png')->generate(utf8_encode($input['name']), public_path('qr-codes')."/$name.png");
//        $ingredient->qr_code = "qr-codes/$name.png";
        $ingredient->update();

        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $ingredient->id;
        $log->tab = 'ingredient';
        $log->type = 'store';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new IngredientsResource($ingredient), 'Ingredients created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $ingredient = Ingredients::find($id);

        if (is_null($ingredient)) {
            return $this->sendError('Ingredient not found');
        }

        return $this->sendResponse(new IngredientsResource($ingredient), 'Ingredient retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Ingredients $ingredient
     * @return JsonResponse
     */
    public function update(Request $request, Ingredients $ingredient)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => Rule::unique('ingredients')->where('deleted_at', NULL)->ignore($ingredient->id),
            'code_1c' => Rule::unique('ingredients')->where('deleted_at', NULL)->ignore($ingredient->id)
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
//        if ($request->get('name')){
//            $name = md5(time());
//            \QrCode::size(500)->format('png')->generate(utf8_encode($input['name']), public_path('qr-codes')."/$name.png");
//            $ingredient->qr_code = "qr-codes/$name.png";
//        }
        $ingredient->update($input);
        $log = new Log();
        $log->user_id = Auth::id();
        $log->resource_id = $ingredient->id;
        $log->tab = 'ingredient';
        $log->type = 'update';
        $log->farm_id = Auth::user()->farm_id;
        $log->save();

        return $this->sendResponse(new IngredientsResource($ingredient), 'Ingredient updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Ingredients $ingredient
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Ingredients $ingredient)
    {
        $recipe = Proportions::where('ingredient_id', $ingredient->id)->pluck('recipes_id');
        if(Recipes::whereIn('id', $recipe)->sum('status') > 0 ){
            return $this->sendError('Validation Error.', ['ingredient in used' => ['The ingredient is used in the active recipe']]);
        }else{
            foreach (collect($recipe) as $id) {
                Recipes::where('id', $id)->update([
                    'closed' => 1,
                ]);
            }
            $log = new Log();
            $log->user_id = Auth::id();
            $log->resource_id = $ingredient->id;
            $log->tab = 'ingredient';
            $log->type = 'delete';
            $log->farm_id = Auth::user()->farm_id;
            $log->save();
            $ingredient->delete();
            return $this->sendResponse([$recipe], 'Ingredient deleted successfully.');
        }
//        $ingredientInRecipes = array_unique(array_merge( Proportions::pluck('ingredient_id')->toArray()));
//
//        if (!in_array($ingredient->id, $ingredientInRecipes)){
//            $log = new Log();
//            $log->user_id = Auth::id();
//            $log->resource_id = $ingredient->id;
//            $log->tab = 'ingredient';
//            $log->type = 'delete';
//            $log->farm_id = Auth::user()->farm_id;
//            $log->save();
//            $ingredient->delete();
//            return $this->sendResponse([], 'Ingredient deleted successfully.');
//        }
//        else
//            return $this->sendError('Validation Error.', 'Рецепти мають в собі інгредієнти');

    }
}
