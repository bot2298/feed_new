<?php

namespace App\Http\Resources;

use App\Models\Permissions;
use Illuminate\Http\Resources\Json\JsonResource;

class SectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'section_id' =>  $this->id,
            'section_name' =>  $this->section_name,
            'users'=> UsersPermissionResourse::collection(Permissions::where('section_id',$this->id)->get())
        ];
    }
}
