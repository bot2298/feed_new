<?php

namespace App\Http\Resources;

use App\Models\Batches;
use App\Models\Downloads;
use App\Models\Recipes;
use App\Models\Silages;
use App\Models\Timetables;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ReportsExportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $planned = Silages::whereIn('id',$this->silages_ids)->sum('planned_weight');
        $batches = Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at');
        $batchIds = $batches->pluck('id');
        $fact = Downloads::whereIn('batch_id',$batchIds)->sum('weight');
        $load_time = $batches->sum('time');


        return [
            'date' =>$this->planned_at,
            'name'=>$this->name,
            'status'=>$this->status,
            'recipe_id'=>$this->recipe_id,
            'recipe_name'=>$this->recipe->name,
            'comment'=>$this->recipe->comment,
            'recipe_start'=>$this->recipe->created_at,
            'plan_weight'=>($this->status == Timetables::STATUS_DONE ||$this->status == Timetables::STATUS_EMPTY)?$batches->sum('plan_weight'):$planned,
            'fact_weight'=>$fact,
            'load_time'=>$load_time,
//            'mixing_time'=>$batches->sum('mixer_time'),
            'batches' => BatchesResource::collection($batches->get()),
            'houses'=>Silages::whereIn('id',$this->silages_ids)->get()->map(function($temp) {
                return [
                    'housing_name'    => $temp->housing_name,
                ];
            })
                ->pluck('housing_name')
                ->all(),
            'silages'=>Silages::whereIn('id',$this->silages_ids)->pluck('number'),
            'operator' => User::find($this->user_id)->name,
            'streets'=>User::whereIn('id',$batches->pluck('user_id'))->pluck('name')
        ];
    }
}
