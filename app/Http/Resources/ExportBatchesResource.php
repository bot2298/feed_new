<?php

namespace App\Http\Resources;

use App\Models\Batches;
use App\Models\Downloads;
use App\Models\Ration;
use App\Models\Recipes;
use App\Models\Silages;
use App\Models\Timetables;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class ExportBatchesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $time = Session::get('time');

        $downloads = Downloads::where('batch_id',$this->id)->whereTime('created_at', '<=', $time);

        $factWeight = $downloads->sum('weight');

        $timetable = Timetables::find($this->timetable_id);

        $houses = Silages::whereIn('id',$timetable->getAttribute('silages_ids'))->get()->map(function($temp) {
            return $temp->housing_name;
        });
        $silages = Silages::whereIn('id',$timetable->getAttribute('silages_ids'))->get()->map(function($temp) {
            return $temp->number;
        });
        $recipe = Recipes::find($timetable->getAttribute('recipe_id'));
        $ration_name = Ration::find($recipe->getAttribute('ration_id'))->name;
        $recipe_name = $recipe->getAttribute('name');
        if($this->loaded_at <= $time){
            $status = 'Done';
        }else{
            $status = 'Process';
        }



        return [
            'status'=>$status,
            'date' =>$this->planned_at,
            'recipe_name'=>$recipe_name,
            'ration_name'=>$ration_name,
            'house_names'=>collect($houses)->implode(', ') ,
            'silages_number'=>collect($silages)->implode(', '),
            'plan_weight'=>$this->plan_weight,
            'fact_weight'=>$factWeight,
            'diff'=>round(1-$factWeight/$this->plan_weight,3)*-1,
            'start'=>Carbon::create($this->loaded_at)->format('H:i') ,
            'duration'=>round((int)($this->time/60),2).':'.$this->time%60,
            'ingredients'=>DownloadResource::collection($downloads->get())
        ];
    }
}
