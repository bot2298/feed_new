<?php

namespace App\Http\Resources;

use App\Models\Accounting;
use Illuminate\Http\Resources\Json\JsonResource;

class AccountingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'ingredient_id'=>$this->ingredient_id,
            'ingredient_name'=>$this->ingredient_name,
            'weight'=>round($this->weight,2),
            'bags'=>round($this->bugs,2),
            'price'=>round($this->price,2),
            'provider_id'=>$this->provider_id,
            'provider_name'=>$this->provider_name,
            'type'=>Accounting::TYPE[$this->type],
            'created_at' => $this->created_at->format('Y-m-d h:m:s'),
            'updated_at' => $this->updated_at->format('Y-m-d h:m:s'),
        ];
    }
}
