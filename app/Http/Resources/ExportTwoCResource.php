<?php

namespace App\Http\Resources;

use App\Models\DoneProducts;
use App\Models\Ingredients;
use App\Models\Proportions;
use App\Models\Ration;
use Illuminate\Http\Resources\Json\JsonResource;

class ExportTwoCResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $propor = Proportions::where('recipes_id', $this->id);
        $ingred_id = $propor->pluck('ingredient_id');
        $ingred_rate = $propor->pluck('rate');
        $ingred_name = Ingredients::whereIn('id', $ingred_id)->pluck('name');
        $ration = Ration::where('id', $this->ration_id)->pluck('code_1c');
        return [
            'id' => $this->id,
            'name' => $this->name,
            'done_prod_id'=>$ration,
            'done_prod_name'=>$this->name,
            'ingredient_id' => $ingred_id,
            'ingredient_rate' => $ingred_rate,
            'ingredient_name' => $ingred_name
        ];
    }
}
