<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PermissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'read'=>boolval($this->read),
            'write'=>boolval($this->write),
            'change'=>boolval($this->change),
            'delete'=>boolval($this->delete),
        ];
    }
}
