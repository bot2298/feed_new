<?php

namespace App\Http\Resources;

use App\Models\Batches;
use App\Models\Recipes;
use Illuminate\Http\Resources\Json\JsonResource;

class TimetablesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $batches_id = Batches::where('timetable_id', $this->id)->pluck('id')??null;
        $ration_id = Recipes::find($this->recipe_id)->ration_id;
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'planned_at'=>$this->planned_at,
            'started_at'=>$this->started_at,
            'ended_at'=>$this->ended_at,
            'recipe_id'=>$this->recipe_id,
            'ration_id'=>$ration_id,
            'ration'=>$this->ration,
            'change'=>$this->change,
            'status'=>$this->status,
            'recipe'=>$this->recipe,
            'mixing_time'=>$this->mixing_time,
            'batches_id'=>$batches_id,
            'download'=>$this->download,
            'silages_list'=>$this->silages_list,
            'housing_list'=>$this->housing_list,
            'created_at' => $this->created_at->format('Y-m-d h:m:s'),
            'updated_at' => $this->updated_at->format('Y-m-d h:m:s'),
        ];
    }
}
