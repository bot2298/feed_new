<?php

namespace App\Http\Resources;

use App\Models\Downloads;
use App\Models\Recipes;
use App\Models\Timetables;
use Illuminate\Http\Resources\Json\JsonResource;

class BatchesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $downloads = Downloads::where('batch_id',$this->id);
        $recipe = Recipes::withoutTrashed()->find(Timetables::withoutTrashed()->find($this->timetable_id)->getAttribute('recipe_id'));
        return [
            'id' =>$this->id,
            'time' =>$this->created_at->format("H:m"),
            'status'=>(is_null($this->loaded_at)? 'in_progress' : 'done'),
            'recipe_id'=>$recipe->id,
            'recipe_name'=>$recipe->name,
            'plan_weight'=> $this->plan_weight,
            'fact_weight'=> $downloads->sum('weight'),
            "load_time"=>$downloads->sum('time'),
            'ingredients'=>DownloadResource::collection($downloads->get()),
            "user_id"=>$this->user_id
        ];
    }
}
