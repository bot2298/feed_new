<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PremixResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ingredient_id' => $this->ingredients_id,
            'need_premix' => $this->need_premix,
            'need_oil' => $this->need_oil
        ];
    }
}
