<?php

namespace App\Http\Resources;

use App\Models\Batches;
use App\Models\Ingredients;
use App\Models\Proportions;
use App\Models\Recipes;
use App\Models\Timetables;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class DownloadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $batch = Batches::find($this->batch_id);
        $recipe = Recipes::withoutTrashed()->find(Timetables::withoutTrashed()->find($batch->timetable_id)->getAttribute('recipe_id'));
        $proportion = Proportions::where('recipes_id',$recipe->id)->where('ingredient_id',$this->ingredient_id)->first();
        $plan = $batch->getAttribute('plan_weight')*$proportion->rate/100;

        return [
            'ingredient_id'=>$this->ingredient_id,
            "ingredient_name"=>Ingredients::withoutTrashed()->find($this->ingredient_id)->getAttribute('name'),
            'plan_weight'=> $plan,
            'fact_weight'=> $this->weight,
            'rate_deviation'=>round(($this->weight-$plan)*100/$plan,3),
            'weight_deviation'=>$this->weight - $plan,
            "time" => Carbon::create($this->loaded_at)->format("H:i"),
            'loaded_time'=>$this->time,
            'user'=>User::withoutTrashed()->find($batch->user_id)->name
        ];
    }
}
