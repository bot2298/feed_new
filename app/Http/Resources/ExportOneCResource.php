<?php

namespace App\Http\Resources;

use App\Models\Batches;
use App\Models\DoneProducts;
use App\Models\Downloads;
use App\Models\Ingredients;
use App\Models\Proportions;
use App\Models\Ration;
use App\Models\Recipes;
use App\Models\Silages;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Validation\Rules\In;

class ExportOneCResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $batch = Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at')->get();
        $batchIds = $batch->pluck('id');
        $fact = Downloads::whereIn('batch_id',$batchIds)->sum('weight');
        $downloads = Downloads::whereIn('batch_id',$batchIds);
        $ingred_ids = $downloads->pluck('ingredient_id')->unique();
        $OneCcode = Ingredients::withTrashed()->pluck('code_1c');
        $recipe_name = Recipes::where('id', $this->recipe_id)->pluck('name');
        $inged_fact = [];
        $ingred_rate = [];
        for($i = 0;$i<count($ingred_ids);$i++){
            $inged_fact[$i] = Downloads::whereIn('batch_id',$batchIds)->where('ingredient_id', $ingred_ids[$i])->sum('weight');
            $ingred_rate[$i] = Proportions::where('recipes_id', $this->recipe_id)->where('ingredient_id', $ingred_ids[$i])->pluck('rate');
        }
        $ration_id = Recipes::where('id', $this->recipe_id)->pluck('ration_id');
        if(is_null($ration_id)){
            $done_code = 'empty';
        }else {
            $done_code = Ration::where('id', $ration_id)->pluck('code_1c');
        }
        return [
            'date' => $this->planned_at,
            'id' => $this->id,
            'weight_done' => $fact,
            'recipe_id' => $this->recipe_id,
            'done_prod'=> $done_code,
            'storage_done_id' => 0,
            'ingredients_id' => $ingred_ids,
            'ingredient_code' => $OneCcode,
            'ingredient_fact' => $inged_fact,
            'recipe_name' => $recipe_name,
            'ingredient_rate' => $ingred_rate,
            'storage_ingredient_id' => 0
        ];
    }
}
