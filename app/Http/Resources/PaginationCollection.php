<?php

namespace App\Http\Resources;

use App\Helpers\CollectionHelper;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PaginationCollection extends ResourceCollection
{
    /**    * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function toArray($request)
    {
        return CollectionHelper::paginate($this->collection);
    }
}
