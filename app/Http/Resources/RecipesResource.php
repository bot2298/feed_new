<?php

namespace App\Http\Resources;

use App\Models\Proportions;
use Illuminate\Http\Resources\Json\JsonResource;

class RecipesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'proportions'=>$this->proportions,
            'fact_price'=>$this->fact_price,
            'status' => boolval($this->status),
            'closed' => boolval($this->closed),
            'comment'=>$this->comment,
            'ration_id'=>$this->ration_id,
            'created_at' => $this->created_at->format('Y-m-d h:m:s'),
            'updated_at' => $this->updated_at->format('Y-m-d h:m:s'),
        ];
    }
}
