<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SilagesResource extends JsonResource
{
    public $preserveKeys = true;
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'housing_id' => $this->housing_id,
            'housing_name'=> $this->housing_name,
            'number' => $this->number,
            'max_weight' => $this->max_weight,
            'net_weight' => $this->net_weight,
            'planned_weight' => $this->planned_weight,
            'comment' => $this->comment,
            'created_at' => $this->created_at->format('Y-m-d h:m:s'),
            'updated_at' => $this->updated_at->format('Y-m-d h:m:s'),
        ];
    }
}
