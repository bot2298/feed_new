<?php

namespace App\Http\Resources;

use App\Models\Batches;
use App\Models\Downloads;
use App\Models\Premix;
use App\Models\Recipes;
use App\Models\Silages;
use App\Models\Timetables;
use Illuminate\Http\Resources\Json\JsonResource;

class ReportsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $premix = Premix::where('timetable_id', $this->id);
        $batches = Batches::where('timetable_id',$this->id);
        $batchIds = $batches->pluck('id');
        $fact = Downloads::whereIn('batch_id',$batchIds)->sum('weight');
        $planned = Silages::whereIn('id',$this->silages_ids)->sum('planned_weight') + Batches::whereIn('id', $batchIds)->where('loaded_at', '!=', Null)->sum('plan_weight');
        $load_time = $batches->sum('time');
        $count = round($planned/1000, 0);
        if($planned < 1000)
            $count = 1;
        if($this->status == 'done' || $this->status == 'uncompleted')
            $count = Batches::where('timetable_id',$this->id)->count();
        $ration = Recipes::find($this->recipe_id);
        return [
            'timetable_id'=>$this->id,
            'premix'=>PremixResource::collection($premix->get()),
            'count'=>$count,
            'date' =>$this->planned_at,
            'name'=>$this->name,
            'status'=>$this->status,
            'download'=>$this->download,
            'recipe_id'=>$this->recipe_id,
            'ration_id'=>$ration->ration_id,
            'ration'=>$this->ration,
            'recipe'=>$this->recipe,
            'recipe_name'=>$this->recipe->name,
            'silages_list'=>$this->silages_list,
            'housing_list'=>$this->housing_list,
            'plan_weight'=>($this->status == Timetables::STATUS_DONE ||$this->status == Timetables::STATUS_EMPTY)?$batches->sum('plan_weight'):$planned,
            'fact_weight'=>$fact,
            'load_time'=>$load_time,
            'batches' => BatchesResource::collection($batches->get())
        ];
    }
}
