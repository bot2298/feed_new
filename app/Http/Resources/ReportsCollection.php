<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReportsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=>$this->name,
            'status'=>$this->status,
            'recipe_id'=>$this->recipe_id,
            'recipe_name'=>$this->recipe,
            'plan_weight'=>$this->plan_weight,
            'fact_weight'=>$this->fact_weight,
            'load_time'=>$this->load_time
        ];
    }
}
