<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProportionsResource extends JsonResource
{

    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ingredient_id' => $this->ingredient_id,
            'ingredient_name' => $this->ingredient_name,
            'rate'=> $this->rate,
            'fact_price'=>$this->fact_price,
        ];
    }
}
