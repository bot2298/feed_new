<?php

namespace App\Http\Resources\Mobile;

use App\Models\Batches;
use App\Models\Downloads;
use App\Models\Ingredients;
use App\Models\Mixers;
use App\Models\Proportions;
use App\Models\Silages;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class TimetablesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $mixing = Mixers::find($request->get('mixer'))->value('max_weight');
        $planned = Silages::whereIn('id',$this->silages_ids)->sum('planned_weight')+Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at')->sum('plan_weight');
        $fact = Downloads::whereIn('batch_id', Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at')->pluck('id'))->sum('weight');

        if ($mixing >= ($planned-$fact)){
            $mixingWeight = $planned-$fact;
        }
        else{
            $mixingWeight = $mixing;
        }

        return array (
            'id' => $this->id,
            'schedule_animalfeed_recipe' =>
                array (

                    'id' => $this->id,
                    'animalfeed_recipe' => $this->recipe_id,
                    'name' => $this->recipe->name,
                    'schedule_ingredients' => $this->getScheduleIngredients($request),
                ),
            'name' => $this->name,
            'plan_weight' => Silages::whereIn('id',$this->silages_ids)->sum('planned_weight') + Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at')->sum('plan_weight'),
            'fact_weight' =>
                Downloads::whereIn('batch_id', Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at')->pluck('id'))->sum('weight'),
            'fact_price' => NULL,
            'is_active' => true,
            'download'=>$this->download,
            'status' => $this->status,
            'change' => boolval($this->change),
            'planned_on' => $this->planned_at,
            'deleted' => false,
            'ordering' => 0,
//            'mix_time' => Mixers::find($request->get('mixer'))->value('mixing_time'),
            'mixing_weight'=> 1000,
            'in_process_weight' => 0.0,
            'started_at' => $this->started_at,
            'ended_at' => $this->ended_at,
            'is_empty' => false,
            'daily_feed_intake' => 0,
            'total_amount' => 0,
        );
    }

    public function getScheduleIngredients($request){

        $proportion = Proportions::where('recipes_id',$this->recipe_id)->get()->toArray();
        foreach ($proportion as $key =>$item){
//            $proportion[$key]['plan_weight'] = Silages::whereIn('id',$this->silages_ids)->sum('planned_weight')*($proportion[$key]['rate']/100)
//                +Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at')->sum('plan_weight')*($proportion[$key]['rate']/100);
//            $proportion[$key]["fact_weight"] =
//                Downloads::where('batch_id', Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at')->latest()->first()->pluck('id'))->where('ingredient_id', $proportion[$key]["ingredient_id"])->sum('weight');
//            $proportion[$key]["diff_in_percent"]= 10;
            $proportion[$key]["auto_load"] = Ingredients::find($proportion[$key]["ingredient_id"])->getAttribute('autoload');



        }
        Session::put('bathes', Batches::where('timetable_id',  $this->id)->latest('id')->whereNotNull('loaded_at')->pluck('id')->first());
        Session::put('yes', true);
        return ProportionsResource::collection($proportion);
    }
}
