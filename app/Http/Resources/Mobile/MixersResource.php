<?php

namespace App\Http\Resources\Mobile;

use App\Models\Calibrations;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MixersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $calibrationsNew =[];
        $calibrations = Calibrations::where('mixer_id',$this->id)->get(["calibration"]);
        foreach ($calibrations as $key => $item){
            $calibrationsNew [$key]['calibration'] = $item->calibration;
            $calibrationsNew [$key]['created_at'] = $item->created_at;
        }

        $calibration = Calibrations::find($this->calibration_id) ? Calibrations::find($this->calibration_id)->getAttribute("calibration"):NULL;

        return [
            'id' => $this->id,
            'name'=>$this->name,
            'min_weight' => $this->min_weight,
            'max_weight' => $this->max_weight,
            'rounding'  =>  false,
//            'skip_time' => $this->mixing_time,
            "up_down" => false,
            'calibration'=> $calibration,
            'calibrations'=> $calibrationsNew,
            'created_at' => $this->created_at->format('Y-m-d h:m:s'),
            'updated_at' => $this->updated_at->format('Y-m-d h:m:s'),
        ];
    }
}
