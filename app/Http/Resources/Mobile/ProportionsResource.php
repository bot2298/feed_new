<?php

namespace App\Http\Resources\Mobile;

use App\Models\Batches;
use App\Models\Downloads;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class ProportionsResource extends JsonResource
{

    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $plan = Batches::where('id', Session::get('bathes'))->sum('plan_weight') * $this['rate']/100;
        $fact = Downloads::where('batch_id', Session::get('bathes'))->where('ingredient_id', $this["ingredient_id"])->sum('weight');
        $load = Downloads::where('batch_id', Session::get('bathes'))->where('ingredient_id', $this["ingredient_id"])->pluck('loaded_at')->implode(', ');
        if($plan == 0)
            $plan = 123;
        if (Session::get('yes'))
            $fact = 0;
            $load = 0;
        return [
            'ingredient' => $this["ingredient_id"],
            'name' => $this['ingredient_name'],
            'auto_load'=>boolval($this['auto_load']),
            "plan_weight"=> round($this["plan_weight"]??$plan,2),
            "fact_weight"=> round($this["fact_weight"]??$fact,2),
            "diff_in_percent"=>round(-(1 - ($this["fact_weight"]??$fact)/($this["plan_weight"]??$plan))*100,2),
            "percent"=> $this['rate'],
            "loaded_at" => $load
        ];
    }
}
