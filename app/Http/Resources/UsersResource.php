<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'login'=>$this->name,
            'full_name' => $this->name,
            'first_name' =>$this->first_name,
            'last_name'=>$this->last_name,
            'group_key'=>$this->group,
            'group_name'=>$this->group,
            'email'=>$this->email,
            'phone'=>$this->phone
        ];
    }
}
