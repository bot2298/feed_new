<?php

namespace App\Http\Resources;

use App\Models\Batches;
use App\Models\Downloads;
use App\Models\Recipes;
use App\Models\Silages;
use App\Models\Timetables;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class exportOnlyRationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $recipes_id = Recipes::where('ration_id', $this->id)->pluck('id');
        $timetables = Timetables::where('recipe_id', $recipes_id);

        if ($request->get('date_from') || $request->get('date_to')) {
            $date_from = new Carbon($request->get('date_from') ?? 'first day of January 2016');
            $date_to = new Carbon($request->get('date_to') ?? 'first day of January 2120');
            $timetables = $timetables->whereBetween('planned_at', [$date_from, $date_to->addDays(1)])->get();
        } else {
//            $timetables = $timetables->whereDate('planned_at', Carbon::now())->get();
            $timetables = $timetables->get();
        }
        if ($request->get('status')) {
            $timetables = $timetables->filter(
                function ($value) use ($request) {
                    return $value->status == $request->get('status');
                }
            );
        }
        if ($request->get('recipe_id')) {
            $timetables = $timetables->filter(
                function ($value) use ($request) {
                    return $value->recipe_id == $request->get('recipe_id');
                }
            );
        }

        return [
            'from' => $request->get('date_from') ?? Carbon::now(),
            'to'=> $request->get('date_to')??Null,
            'name' => $this->name,
            'recipes' => Recipes::where('ration_id', $this->id)->pluck('name'),
            'plan' => Batches::whereIn('timetable_id', $timetables->pluck('id'))->sum('plan_weight'),
            'silages' => Silages::whereIn('id', $timetables->pluck('silages_ids'))->pluck('number'),
            'houses'=>Silages::whereIn('id',$timetables->pluck('silages_ids'))->get()->map(function($temp) {
                return [
                    'housing_name'    => $temp->housing_name,
                ];
            })
                ->pluck('housing_name')
                ->unique(),
            'fact' => Downloads::whereIn('batch_id', Batches::whereIn('timetable_id', $timetables->pluck('id'))->pluck('id'))->sum('weight')
        ];
    }
}
