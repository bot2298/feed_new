<?php

namespace App\Http\Resources;

use App\Models\Calibrations;
use Illuminate\Http\Resources\Json\JsonResource;

class MixersResource extends JsonResource
{

    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name'=>$this->name,
            'min_weight' => $this->min_weight,
            'max_weight' => $this->max_weight,
//            'mixing_time'=> $this->mixing_time,
            'calibration_at'=>Calibrations::where('id',$this->calibration_id)->value('created_at'),
            'calibration_list'=> Calibrations::where('mixer_id',$this->id)->get(['id',"calibration","created_at"]),
            'weight_up'=>$this->weight_up,
            'rounding'=>$this->rounding,
            'auto_load'=>$this->auto_load,
            'created_at' => $this->created_at->format('Y-m-d h:m:s'),
            'updated_at' => $this->updated_at->format('Y-m-d h:m:s'),
        ];
    }
}
