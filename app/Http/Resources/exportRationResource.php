<?php

namespace App\Http\Resources;

use App\Models\Batches;
use App\Models\Downloads;
use App\Models\Ingredients;
use App\Models\Proportions;
use App\Models\Ration;
use App\Models\Recipes;
use Illuminate\Http\Resources\Json\JsonResource;

class exportRationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ration_id = Recipes::find($this->recipe_id)->ration_id;
        if($ration_id == 0){
            $ration_name = '';
        }else {
            $ration_name = Ration::find($ration_id)->name;
        }
        $recipe_name = Recipes::find($this->recipe_id)->name;

        $batch = Batches::where('timetable_id',$this->id)->whereNotNull('loaded_at')->pluck('id');
        $ingred_ids = Downloads::whereIn('batch_id',$batch)->pluck('ingredient_id')->unique();
        $inged_fact = [];
        $inged_plan = [];
        $inged_name = [];
        $def = [];
        $batch_plan = Batches::where('timetable_id',$this->id)->sum('plan_weight');
        for($i = 0;$i<count($ingred_ids);$i++) {
            $ingred_rate = Proportions::where('recipes_id', $this->recipe_id)->where('ingredient_id', $ingred_ids[$i])->sum('rate');
            $inged_plan[$i] = $batch_plan * $ingred_rate / 100;
            $inged_name[$i] = Ingredients::find($ingred_ids[$i])->name;
            $inged_fact[$i] = Downloads::whereIn('batch_id', $batch)->where('ingredient_id', $ingred_ids[$i])->sum('weight');
            $def[$i] = $inged_fact[$i] - $inged_plan[$i];
        }


        return [
            'date' => $this->planned_at,
            'ration' => $ration_name,
            'recipe' => $recipe_name,
            'ingredient' => $ingred_ids,
            'ing_name' => $inged_name,
            'ing_plan' => $inged_plan,
            'ing_fact' => $inged_fact,
            'def' => $def,
        ];
    }
}
