<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IngredientsResource extends JsonResource
{

    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'qr_code'=>env('APP_URL').$this->qr_code,
            'code_1c'=>$this->code_1c,
            'weight_per_day'=>round($this->weight_per_day,2),
            'remainder_day'=>round($this->remainder_day,2),
            'remainder_weight'=>round($this->remainder_weight,2),
            'remainder_bags'=>round($this->remainder_bags,2),
            'cost'=>round($this->cost,2),
            'autoload'=>boolval($this->autoload),
            'created_at' => $this->created_at->format('Y-m-d h:m:s'),
            'updated_at' => $this->updated_at->format('Y-m-d h:m:s'),
        ];
    }
}
