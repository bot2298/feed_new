<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFildsToCalibrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calibrations', function (Blueprint $table) {
            $table->dropColumn('needless_weight');
            $table->dropColumn('units_per_one_kg');
            $table->string('calibration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calibrations', function (Blueprint $table) {
            $table->double('needless_weight');
            $table->double('units_per_one_kg');
            $table->string('units_per_one_kg');
        });
    }
}
