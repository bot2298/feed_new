<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('batches', function (Blueprint $table) {
            $table->bigInteger('user_id');
            $table->bigInteger('timetable_id');
            $table->float('plan_weight');
            $table->dateTime('loaded_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('batches', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('timetable_id');
            $table->dropColumn('plan_weight');
            $table->dropColumn('loaded_at');
        });
    }
}
