<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premix', function (Blueprint $table) {
            $table->bigInteger('timetable_id');
            $table->bigInteger('ingredients_id');
            $table->boolean('need_premix')->default(False);
            $table->boolean('need_oil')->default(False);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premix');
    }
}
