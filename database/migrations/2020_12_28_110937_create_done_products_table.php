<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoneProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('done_product', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('name');
            $table->bigInteger('weight');
            $table->float('cost');
            $table->timestamps();
            $table->bigInteger('farm_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('done_product');
    }
}
