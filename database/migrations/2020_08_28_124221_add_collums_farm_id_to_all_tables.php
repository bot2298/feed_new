<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCollumsFarmIdToAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->bigInteger('farm_id');
        });
        Schema::table('mixers', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
        Schema::table('housings', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
        Schema::table('silages', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
        Schema::table('providers', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
        Schema::table('ingredients', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
        Schema::table('accountings', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
        Schema::table('recipes', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
        Schema::table('timetables', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->bigInteger('farm_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('mixers', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('housings', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('silages', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('ingredients', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('accountings', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('recipes', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('timetables', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->dropColumn('farm_id');
        });
    }
}
