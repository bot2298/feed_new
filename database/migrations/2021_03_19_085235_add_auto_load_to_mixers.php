<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAutoLoadToMixers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mixers', function (Blueprint $table) {
            $table->boolean('auto_load')->default(null);
            $table->boolean('weight_up')->default(0);
            $table->boolean('rounding')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mixers', function (Blueprint $table) {
            //
        });
    }
}
