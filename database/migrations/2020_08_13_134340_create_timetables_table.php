<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetables', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamp('planned_at');
            $table->timestamp('started_at',0)->nullable();
            $table->timestamp('ended_at',0)->nullable();
            $table->integer('recipe_id');
            $table->json('silages_ids')->nullable();
            $table->boolean('change')->default(0);
            $table->string('status')->default('empty');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetables');
    }
}
