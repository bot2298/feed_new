<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSilagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('silages', function (Blueprint $table) {
            $table->id();
            $table->integer('housing_id')->unsigned()->index();
            $table->string('number')->unique();
            $table->float('max_weight')->default(0);
            $table->float('net_weight')->default(0);
            $table->float('planned_weight')->default(0);
            $table->string('comment')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('silages');
    }
}
