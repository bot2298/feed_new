<?php

use App\Models\Providers;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $sections = [
            'Планування',
            'Рецепти',
            'Інгредієнти',
            'Прийом',
            'Списання',
            'Постачальники',
            'Співробітники',
            'Історія',
            'Звіти',
            'Силоси',
            'Міксери',
        ];
        $keys = [
            'timetable',
            'recipes',
            'ingredients',
            'retentions',
            'write-off',
            'providers',
            'users',
            'logs',
            'reports',
            'silages',
            'mixers'
        ];
        $temp =array_combine($keys,$sections);

        foreach ($temp as $keys => $item){
            \App\Models\Section::create([
                'section_name' => $item,
                'sections_key'=>$keys]);
        }

//        $providerSelf = [
//            'name'=>'self',
//            'type'=>'self',
//            'info'=>'self'
//        ];
//        Providers::create($providerSelf);

    }
}
